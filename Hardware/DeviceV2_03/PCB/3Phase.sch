EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 17000 11000
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 3Phase-rescue:R-3Phase-rescue R14
U 1 1 5BD04949
P 3650 3725
F 0 "R14" V 3575 3725 50  0000 C CNN
F 1 "100h" V 3650 3725 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 3580 3725 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_100R-100R-1_C125923.html" H 3650 3725 50  0001 C CNN
	1    3650 3725
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R27
U 1 1 5BD04F97
P 1275 1575
F 0 "R27" V 1355 1575 50  0000 C CNN
F 1 "200k" V 1275 1575 50  0001 C CNN
F 2 "Footprints:R_1206_3216Metric" V 1205 1575 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_AC1206FR-07200KL_C144506.html" H 1275 1575 50  0001 C CNN
	1    1275 1575
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R30
U 1 1 5BD0515E
P 1600 1575
F 0 "R30" V 1680 1575 50  0000 C CNN
F 1 "200k" V 1600 1575 50  0001 C CNN
F 2 "Footprints:R_1206_3216Metric" V 1530 1575 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_AC1206FR-07200KL_C144506.html" H 1600 1575 50  0001 C CNN
	1    1600 1575
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R32
U 1 1 5BD05222
P 1950 1925
F 0 "R32" V 1875 1925 50  0000 C CNN
F 1 "2.2k" V 1950 1925 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1880 1925 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1950 1925 50  0001 C CNN
	1    1950 1925
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C7
U 1 1 5BD06419
P 3850 3850
F 0 "C7" H 3675 3850 50  0000 L CNN
F 1 "100nF 25v" H 3860 3770 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3850 3850 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3850 3850 50  0001 C CNN
	1    3850 3850
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:CONN_01X04-3Phase-rescue J1
U 1 1 5BD094EB
P 875 3925
F 0 "J1" H 875 4175 50  0000 C CNN
F 1 "95001-6P6C" H 675 4275 50  0001 C CNN
F 2 "Footprints:KF2EDGR_2.54x4" H 875 3925 50  0001 C CNN
F 3 "" H 875 3925 50  0001 C CNN
	1    875  3925
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1425 1575 1450 1575
$Comp
L 3Phase-rescue:SI8921BB-Components U6
U 1 1 60C6672C
P 2550 950
F 0 "U6" H 2525 1200 50  0000 C CNN
F 1 "SI8921BB" H 2662 1226 50  0001 C CNN
F 2 "Footprints:SOIC-8_3.9x4.9mm_P1.27mm" H 2700 600 50  0001 L CIN
F 3 "" H 2695 955 50  0001 L CNN
	1    2550 950 
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:SI8921BB-Components U14
U 1 1 60C6A971
P 2550 1875
F 0 "U14" H 2525 2125 50  0000 C CNN
F 1 "SI8921BB" H 2662 2151 50  0001 C CNN
F 2 "Footprints:SOIC-8_3.9x4.9mm_P1.27mm" H 2700 1525 50  0001 L CIN
F 3 "" H 2695 1880 50  0001 L CNN
	1    2550 1875
	1    0    0    -1  
$EndComp
Wire Wire Line
	2325 900  2375 900 
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C43
U 1 1 60ED639F
P 2125 1750
F 0 "C43" H 2200 1750 50  0000 L CNN
F 1 "100nF 25v" H 2135 1670 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 2125 1750 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 2125 1750 50  0001 C CNN
	1    2125 1750
	-1   0    0    1   
$EndComp
Wire Wire Line
	2100 1925 2125 1925
$Comp
L 3Phase-rescue:R-3Phase-rescue R34
U 1 1 60F502AF
P 1775 1750
F 0 "R34" V 1855 1750 50  0000 C CNN
F 1 "2.2k" V 1775 1750 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1705 1750 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1775 1750 50  0001 C CNN
	1    1775 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 1925 1775 1925
Wire Wire Line
	1775 1925 1775 1900
Wire Wire Line
	2125 1850 2125 1925
Connection ~ 2125 1925
Wire Wire Line
	2125 1925 2375 1925
$Comp
L 3Phase-rescue:R-3Phase-rescue R68
U 1 1 6111A63B
P 1950 1575
F 0 "R68" V 2030 1575 50  0000 C CNN
F 1 "2.2k" V 1950 1575 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1880 1575 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1950 1575 50  0001 C CNN
	1    1950 1575
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 1575 1775 1575
Wire Wire Line
	1775 1575 1775 1600
Wire Wire Line
	2100 1575 2125 1575
Wire Wire Line
	2125 1575 2125 1650
Wire Wire Line
	2125 1575 2325 1575
Wire Wire Line
	2325 1575 2325 1825
Wire Wire Line
	2325 1825 2375 1825
Connection ~ 2125 1575
Wire Wire Line
	1750 1575 1775 1575
Connection ~ 1775 1575
Wire Wire Line
	2375 2025 1775 2025
Wire Wire Line
	1775 2025 1775 1925
Connection ~ 1775 1925
Wire Wire Line
	2250 1300 2275 1300
Wire Wire Line
	2250 800  2375 800 
Wire Wire Line
	2150 1300 2250 1300
Wire Wire Line
	1950 1300 1775 1300
Connection ~ 1775 1300
$Comp
L Components:B0505D-1W U17
U 1 1 631FBD24
P 2650 2275
F 0 "U17" H 2425 2100 50  0000 C CNN
F 1 "B0505D-1W" H 2662 2476 50  0001 C CNN
F 2 "Footprints:Converter_DCDC_muRata_CRE1xxxxxx3C_THT" H 2650 1875 50  0001 C CNN
F 3 "http://power.murata.com/datasheet?/data/power/ncl/kdc_cre1.pdf" H 2650 1775 50  0001 C CNN
	1    2650 2275
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1775 2325 1775 2225
Connection ~ 1775 2025
Wire Wire Line
	2375 1725 2250 1725
Wire Wire Line
	2250 2225 2275 2225
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C9
U 1 1 632F85BD
P 1950 2225
F 0 "C9" V 2000 2275 50  0000 L CNN
F 1 "100nF 25v" H 1960 2145 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 1950 2225 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 1950 2225 50  0001 C CNN
	1    1950 2225
	0    1    1    0   
$EndComp
Wire Wire Line
	2050 2225 2250 2225
Connection ~ 2250 2225
Wire Wire Line
	1850 2225 1775 2225
Connection ~ 1775 2225
$Comp
L 3Phase-rescue:R-3Phase-rescue R26
U 1 1 639150E9
P 1275 2500
F 0 "R26" V 1355 2500 50  0000 C CNN
F 1 "200k" V 1275 2500 50  0001 C CNN
F 2 "Footprints:R_1206_3216Metric" V 1205 2500 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_AC1206FR-07200KL_C144506.html" H 1275 2500 50  0001 C CNN
	1    1275 2500
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R28
U 1 1 639150F3
P 1600 2500
F 0 "R28" V 1680 2500 50  0000 C CNN
F 1 "200k" V 1600 2500 50  0001 C CNN
F 2 "Footprints:R_1206_3216Metric" V 1530 2500 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/_AC1206FR-07200KL_C144506.html" H 1600 2500 50  0001 C CNN
	1    1600 2500
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R70
U 1 1 639150FD
P 1950 2850
F 0 "R70" V 1875 2850 50  0000 C CNN
F 1 "2.2k" V 1950 2850 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1880 2850 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1950 2850 50  0001 C CNN
	1    1950 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	1425 2500 1450 2500
$Comp
L 3Phase-rescue:SI8921BB-Components U15
U 1 1 6391510B
P 2550 2800
F 0 "U15" H 2525 3050 50  0000 C CNN
F 1 "SI8921BB" H 2662 3076 50  0001 C CNN
F 2 "Footprints:SOIC-8_3.9x4.9mm_P1.27mm" H 2700 2450 50  0001 L CIN
F 3 "" H 2695 2805 50  0001 L CNN
	1    2550 2800
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C44
U 1 1 63915115
P 2125 2675
F 0 "C44" H 2200 2675 50  0000 L CNN
F 1 "100nF 25v" H 2135 2595 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 2125 2675 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 2125 2675 50  0001 C CNN
	1    2125 2675
	-1   0    0    1   
$EndComp
Wire Wire Line
	2100 2850 2125 2850
$Comp
L 3Phase-rescue:R-3Phase-rescue R60
U 1 1 63915120
P 1775 2675
F 0 "R60" V 1855 2675 50  0000 C CNN
F 1 "2.2k" V 1775 2675 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1705 2675 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1775 2675 50  0001 C CNN
	1    1775 2675
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 2850 1775 2850
Wire Wire Line
	1775 2850 1775 2825
Wire Wire Line
	2125 2775 2125 2850
Connection ~ 2125 2850
Wire Wire Line
	2125 2850 2375 2850
$Comp
L 3Phase-rescue:R-3Phase-rescue R69
U 1 1 6391512F
P 1950 2500
F 0 "R69" V 2030 2500 50  0000 C CNN
F 1 "2.2k" V 1950 2500 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1880 2500 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1950 2500 50  0001 C CNN
	1    1950 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 2500 1775 2500
Wire Wire Line
	1775 2500 1775 2525
Wire Wire Line
	2100 2500 2125 2500
Wire Wire Line
	2125 2500 2125 2575
Wire Wire Line
	2125 2500 2325 2500
Wire Wire Line
	2325 2500 2325 2750
Wire Wire Line
	2325 2750 2375 2750
Connection ~ 2125 2500
Wire Wire Line
	1750 2500 1775 2500
Connection ~ 1775 2500
Wire Wire Line
	2375 2950 1775 2950
Wire Wire Line
	1775 2950 1775 2850
Connection ~ 1775 2850
$Comp
L Components:B0505D-1W U18
U 1 1 63915146
P 2650 3200
F 0 "U18" H 2425 3025 50  0000 C CNN
F 1 "B0505D-1W" H 2662 3401 50  0001 C CNN
F 2 "Footprints:Converter_DCDC_muRata_CRE1xxxxxx3C_THT" H 2650 2800 50  0001 C CNN
F 3 "http://power.murata.com/datasheet?/data/power/ncl/kdc_cre1.pdf" H 2650 2700 50  0001 C CNN
	1    2650 3200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1775 3250 1775 3150
Connection ~ 1775 2950
Wire Wire Line
	2375 2650 2250 2650
Wire Wire Line
	2250 3150 2275 3150
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C38
U 1 1 63915156
P 1950 3150
F 0 "C38" V 2000 3200 50  0000 L CNN
F 1 "100nF 25v" H 1960 3070 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 1950 3150 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 1950 3150 50  0001 C CNN
	1    1950 3150
	0    1    1    0   
$EndComp
Wire Wire Line
	2050 3150 2250 3150
Connection ~ 2250 3150
Wire Wire Line
	1850 3150 1775 3150
Connection ~ 1775 3150
$Comp
L Components:Conn_01x02 P3
U 1 1 63F17E36
P 875 1825
F 0 "P3" H 875 2075 50  0000 C CNN
F 1 "3.81x4 terminal block" V 975 1825 50  0001 C CNN
F 2 "Footprints:PhoenixContact_MC_1,5_2-G-3.81_1x02_P3.81mm_Horizontal" H 875 1825 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Pluggable-System-Terminal-Block_Ningbo-Kangnex-Elec-WJ15EDGRC-3-81-4P_C7245.html" H 875 1825 50  0001 C CNN
	1    875  1825
	-1   0    0    1   
$EndComp
Wire Wire Line
	1075 1925 1775 1925
Wire Wire Line
	1100 1575 1100 1825
Wire Wire Line
	1100 1825 1075 1825
$Comp
L Components:Conn_01x02 P4
U 1 1 640ED55E
P 875 2750
F 0 "P4" H 875 3000 50  0000 C CNN
F 1 "3.81x4 terminal block" V 975 2750 50  0001 C CNN
F 2 "Footprints:PhoenixContact_MC_1,5_2-G-3.81_1x02_P3.81mm_Horizontal" H 875 2750 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Pluggable-System-Terminal-Block_Ningbo-Kangnex-Elec-WJ15EDGRC-3-81-4P_C7245.html" H 875 2750 50  0001 C CNN
	1    875  2750
	-1   0    0    1   
$EndComp
Wire Wire Line
	1075 2850 1775 2850
Wire Wire Line
	1100 2750 1100 2500
Wire Wire Line
	1100 2750 1075 2750
$Comp
L 3Phase-rescue:R-3Phase-rescue R71
U 1 1 665E2127
P 3275 750
F 0 "R71" V 3355 750 50  0000 C CNN
F 1 "12" V 3275 750 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 3205 750 50  0001 C CNN
F 3 "" H 3275 750 50  0001 C CNN
	1    3275 750 
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R72
U 1 1 665E4489
P 3275 1000
F 0 "R72" V 3200 1000 50  0000 C CNN
F 1 "12" V 3275 1000 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 3205 1000 50  0001 C CNN
F 3 "" H 3275 1000 50  0001 C CNN
	1    3275 1000
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C46
U 1 1 665E4C6A
P 3450 875
F 0 "C46" H 3550 875 50  0000 L CNN
F 1 "330pF NP0" H 3460 795 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3450 875 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3450 875 50  0001 C CNN
	1    3450 875 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 1000 3125 1000
Wire Wire Line
	2950 900  3000 900 
Wire Wire Line
	3000 900  3000 750 
Wire Wire Line
	3000 750  3125 750 
Wire Wire Line
	3425 750  3450 750 
Wire Wire Line
	3450 750  3450 775 
Wire Wire Line
	3425 1000 3450 1000
Wire Wire Line
	3450 1000 3450 975 
Wire Wire Line
	3025 1300 3075 1300
Wire Wire Line
	3075 1300 3075 800 
Wire Wire Line
	3075 800  2950 800 
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C45
U 1 1 66B2A491
P 3200 1300
F 0 "C45" V 3100 1250 50  0000 L CNN
F 1 "100nF 25v" H 3210 1220 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3200 1300 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3200 1300 50  0001 C CNN
	1    3200 1300
	0    1    1    0   
$EndComp
Wire Wire Line
	3100 1300 3075 1300
Connection ~ 3075 1300
Wire Wire Line
	2950 1100 3325 1100
Wire Wire Line
	3325 1100 3325 1300
Wire Wire Line
	3325 1400 3025 1400
Wire Wire Line
	3300 1300 3325 1300
Connection ~ 3325 1300
Wire Wire Line
	3325 1300 3325 1400
Wire Wire Line
	3350 1400 3325 1400
Connection ~ 3325 1400
Wire Wire Line
	3075 625  3075 800 
Connection ~ 3075 800 
Text GLabel 3475 750  2    50   Input ~ 0
IN_1P
Wire Wire Line
	3475 750  3450 750 
Connection ~ 3450 750 
Wire Wire Line
	3475 1000 3450 1000
Connection ~ 3450 1000
$Comp
L power:GND #PWR0127
U 1 1 67137176
P 3350 1400
F 0 "#PWR0127" H 3350 1150 50  0001 C CNN
F 1 "GND" H 3350 1250 50  0000 C CNN
F 2 "" H 3350 1400 50  0000 C CNN
F 3 "" H 3350 1400 50  0000 C CNN
	1    3350 1400
	0    -1   -1   0   
$EndComp
$Comp
L Components:B0505D-1W U16
U 1 1 615D213A
P 2650 1350
F 0 "U16" H 2425 1175 50  0000 C CNN
F 1 "B0505D-1W" H 2662 1551 50  0001 C CNN
F 2 "Footprints:Converter_DCDC_muRata_CRE1xxxxxx3C_THT" H 2650 950 50  0001 C CNN
F 3 "http://power.murata.com/datasheet?/data/power/ncl/kdc_cre1.pdf" H 2650 850 50  0001 C CNN
	1    2650 1350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1775 1400 2275 1400
Wire Wire Line
	1775 1300 1775 1400
Connection ~ 2250 1300
Wire Wire Line
	1100 1575 1125 1575
Wire Wire Line
	1775 2025 1775 2225
Wire Wire Line
	2250 1725 2250 2225
Wire Wire Line
	1100 2500 1125 2500
Wire Wire Line
	1775 2950 1775 3150
Wire Wire Line
	2250 2650 2250 3150
Wire Wire Line
	1775 3250 2275 3250
Wire Wire Line
	1775 2325 2275 2325
$Comp
L 3Phase-rescue:R-3Phase-rescue R18
U 1 1 69D44727
P 3275 1675
F 0 "R18" V 3355 1675 50  0000 C CNN
F 1 "12" V 3275 1675 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 3205 1675 50  0001 C CNN
F 3 "" H 3275 1675 50  0001 C CNN
	1    3275 1675
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R21
U 1 1 69D4472D
P 3275 1925
F 0 "R21" V 3200 1925 50  0000 C CNN
F 1 "12" V 3275 1925 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 3205 1925 50  0001 C CNN
F 3 "" H 3275 1925 50  0001 C CNN
	1    3275 1925
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C8
U 1 1 69D44733
P 3450 1800
F 0 "C8" H 3550 1800 50  0000 L CNN
F 1 "330pF NP0" H 3460 1720 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3450 1800 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3450 1800 50  0001 C CNN
	1    3450 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 1925 3125 1925
Wire Wire Line
	2950 1825 3000 1825
Wire Wire Line
	3000 1825 3000 1675
Wire Wire Line
	3000 1675 3125 1675
Wire Wire Line
	3425 1675 3450 1675
Wire Wire Line
	3450 1675 3450 1700
Wire Wire Line
	3425 1925 3450 1925
Wire Wire Line
	3450 1925 3450 1900
Wire Wire Line
	3075 2225 3075 1725
Wire Wire Line
	3075 1725 2950 1725
Wire Wire Line
	2950 2025 3325 2025
Wire Wire Line
	3325 2025 3325 2225
Wire Wire Line
	3075 1300 3075 1725
Connection ~ 3075 1725
Text GLabel 3475 1675 2    50   Input ~ 0
IN_2P
Text GLabel 3475 1925 2    50   Input ~ 0
IN_2N
Wire Wire Line
	3475 1675 3450 1675
Connection ~ 3450 1675
Wire Wire Line
	3475 1925 3450 1925
Connection ~ 3450 1925
$Comp
L 3Phase-rescue:R-3Phase-rescue R23
U 1 1 69DB96E6
P 3275 2600
F 0 "R23" V 3355 2600 50  0000 C CNN
F 1 "12" V 3275 2600 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 3205 2600 50  0001 C CNN
F 3 "" H 3275 2600 50  0001 C CNN
	1    3275 2600
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R29
U 1 1 69DB96EC
P 3275 2850
F 0 "R29" V 3200 2850 50  0000 C CNN
F 1 "12" V 3275 2850 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 3205 2850 50  0001 C CNN
F 3 "" H 3275 2850 50  0001 C CNN
	1    3275 2850
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C10
U 1 1 69DB96F2
P 3450 2725
F 0 "C10" H 3550 2725 50  0000 L CNN
F 1 "330pF NP0" H 3460 2645 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3450 2725 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3450 2725 50  0001 C CNN
	1    3450 2725
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 2850 3125 2850
Wire Wire Line
	2950 2750 3000 2750
Wire Wire Line
	3000 2750 3000 2600
Wire Wire Line
	3000 2600 3125 2600
Wire Wire Line
	3425 2600 3450 2600
Wire Wire Line
	3450 2600 3450 2625
Wire Wire Line
	3425 2850 3450 2850
Wire Wire Line
	3450 2850 3450 2825
Wire Wire Line
	3075 3150 3075 2650
Wire Wire Line
	3075 2650 2950 2650
Wire Wire Line
	2950 2950 3325 2950
Wire Wire Line
	3325 2950 3325 3150
Wire Wire Line
	3075 2225 3075 2650
Connection ~ 3075 2650
Text GLabel 3475 2600 2    50   Input ~ 0
IN_3P
Text GLabel 3475 2850 2    50   Input ~ 0
IN_3N
Wire Wire Line
	3475 2600 3450 2600
Connection ~ 3450 2600
Wire Wire Line
	3475 2850 3450 2850
Connection ~ 3450 2850
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C3
U 1 1 69E3DE31
P 3200 2225
F 0 "C3" V 3100 2175 50  0000 L CNN
F 1 "100nF 25v" H 3210 2145 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3200 2225 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3200 2225 50  0001 C CNN
	1    3200 2225
	0    1    1    0   
$EndComp
Wire Wire Line
	3325 2325 3025 2325
Wire Wire Line
	3300 2225 3325 2225
Connection ~ 3325 2225
Wire Wire Line
	3325 2225 3325 2325
Wire Wire Line
	3350 2325 3325 2325
Connection ~ 3325 2325
$Comp
L power:GND #PWR0129
U 1 1 69E3DE3E
P 3350 2325
F 0 "#PWR0129" H 3350 2075 50  0001 C CNN
F 1 "GND" H 3350 2175 50  0000 C CNN
F 2 "" H 3350 2325 50  0000 C CNN
F 3 "" H 3350 2325 50  0000 C CNN
	1    3350 2325
	0    -1   -1   0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C5
U 1 1 69EB74D8
P 3200 3150
F 0 "C5" V 3100 3100 50  0000 L CNN
F 1 "100nF 25v" H 3210 3070 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3200 3150 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3200 3150 50  0001 C CNN
	1    3200 3150
	0    1    1    0   
$EndComp
Wire Wire Line
	3325 3250 3025 3250
Wire Wire Line
	3300 3150 3325 3150
Connection ~ 3325 3150
Wire Wire Line
	3325 3150 3325 3250
Wire Wire Line
	3350 3250 3325 3250
Connection ~ 3325 3250
$Comp
L power:GND #PWR0130
U 1 1 69EB74E5
P 3350 3250
F 0 "#PWR0130" H 3350 3000 50  0001 C CNN
F 1 "GND" H 3350 3100 50  0000 C CNN
F 2 "" H 3350 3250 50  0000 C CNN
F 3 "" H 3350 3250 50  0000 C CNN
	1    3350 3250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3025 2225 3075 2225
Connection ~ 3075 2225
Wire Wire Line
	3075 2225 3100 2225
Wire Wire Line
	3025 3150 3075 3150
Connection ~ 3075 3150
Wire Wire Line
	3075 3150 3100 3150
Text GLabel 3475 625  2    50   Input ~ 0
5V
$Comp
L 3Phase-rescue:R-3Phase-rescue R33
U 1 1 60DFF1B1
P 1300 4150
F 0 "R33" V 1380 4150 50  0000 C CNN
F 1 "2.2k" V 1300 4150 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1230 4150 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1300 4150 50  0001 C CNN
	1    1300 4150
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R36
U 1 1 60E004E1
P 1475 4150
F 0 "R36" V 1555 4150 50  0000 C CNN
F 1 "2.2k" V 1475 4150 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1405 4150 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1475 4150 50  0001 C CNN
	1    1475 4150
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R40
U 1 1 60E00DFD
P 1650 3725
F 0 "R40" V 1730 3725 50  0000 C CNN
F 1 "2.2k" V 1650 3725 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1580 3725 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1650 3725 50  0001 C CNN
	1    1650 3725
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R56
U 1 1 60E01908
P 1650 3975
F 0 "R56" V 1575 3975 50  0000 C CNN
F 1 "2.2k" V 1650 3975 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1580 3975 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1650 3975 50  0001 C CNN
	1    1650 3975
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C11
U 1 1 60E022C7
P 2050 3850
F 0 "C11" H 2125 3850 50  0000 L CNN
F 1 "100nF 25v" H 2060 3770 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 2050 3850 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 2050 3850 50  0001 C CNN
	1    2050 3850
	-1   0    0    1   
$EndComp
Wire Wire Line
	1800 3725 2050 3725
Wire Wire Line
	2050 3750 2050 3725
Wire Wire Line
	1800 3975 2050 3975
Wire Wire Line
	2050 3950 2050 3975
Wire Wire Line
	2050 3975 2075 3975
Wire Wire Line
	1475 4000 1475 3975
Wire Wire Line
	1500 3975 1475 3975
Wire Wire Line
	1300 4000 1300 3875
Wire Wire Line
	1300 3725 1500 3725
Wire Wire Line
	1075 4075 1200 4075
Wire Wire Line
	1200 4075 1200 4375
Wire Wire Line
	1200 4375 1300 4375
Wire Wire Line
	1300 4300 1300 4375
Connection ~ 1300 4375
Wire Wire Line
	1300 4375 1475 4375
Wire Wire Line
	1475 4375 1475 4300
Wire Wire Line
	1075 3975 1475 3975
Connection ~ 1475 3975
Wire Wire Line
	1075 3875 1300 3875
Connection ~ 1300 3875
Wire Wire Line
	1300 3875 1300 3725
Wire Wire Line
	1075 3775 1200 3775
Wire Wire Line
	1200 3775 1200 3575
Text GLabel 3875 3725 2    50   Input ~ 0
IN_4P
Wire Wire Line
	3800 3725 3850 3725
Wire Wire Line
	3850 3750 3850 3725
Connection ~ 3850 3725
Text GLabel 3375 3850 2    50   Input ~ 0
1.25V
Connection ~ 2050 3725
Connection ~ 2050 3975
Connection ~ 1475 4375
$Comp
L power:GND #PWR0112
U 1 1 623859C1
P 1475 4575
F 0 "#PWR0112" H 1475 4325 50  0001 C CNN
F 1 "GND" H 1475 4425 50  0000 C CNN
F 2 "" H 1475 4575 50  0000 C CNN
F 3 "" H 1475 4575 50  0000 C CNN
	1    1475 4575
	1    0    0    -1  
$EndComp
Text GLabel 1300 3575 2    50   Input ~ 0
5V
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C6
U 1 1 627837F8
P 3350 4250
F 0 "C6" H 3360 4320 50  0000 L CNN
F 1 "100nF 25v" H 3360 4170 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3350 4250 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3350 4250 50  0001 C CNN
	1    3350 4250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2450 10175 2475 10175
Wire Wire Line
	2450 9975 2475 9975
Text GLabel 2475 9975 2    50   Input ~ 0
Vin
$Comp
L power:GND #PWR0107
U 1 1 61698A5E
P 2475 10175
F 0 "#PWR0107" H 2475 9925 50  0001 C CNN
F 1 "GND" V 2375 10225 50  0000 R CNN
F 2 "" H 2475 10175 50  0001 C CNN
F 3 "" H 2475 10175 50  0001 C CNN
	1    2475 10175
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4625 2225 4700 2225
Wire Wire Line
	4625 2125 4700 2125
Wire Wire Line
	4625 2025 4700 2025
Wire Wire Line
	4625 1925 4700 1925
Connection ~ 4650 1825
Wire Wire Line
	4650 1825 4700 1825
Wire Wire Line
	4650 1825 4625 1825
Wire Wire Line
	4650 1725 4650 1825
Wire Wire Line
	4625 1725 4650 1725
Wire Wire Line
	4625 2325 4700 2325
Wire Wire Line
	4700 2425 4625 2425
Wire Wire Line
	4625 2525 4700 2525
Connection ~ 4650 1625
Wire Wire Line
	5225 1625 5175 1625
Wire Wire Line
	4650 1625 4625 1625
Wire Wire Line
	4650 1525 4650 1625
Wire Wire Line
	4625 1525 4650 1525
Connection ~ 4650 1425
Wire Wire Line
	4700 1425 4650 1425
Wire Wire Line
	4650 1425 4625 1425
Wire Wire Line
	4650 1325 4650 1425
Wire Wire Line
	4625 1325 4650 1325
Wire Wire Line
	4625 1225 4700 1225
Wire Wire Line
	4625 1125 4700 1125
Wire Wire Line
	4625 1025 4700 1025
$Comp
L power:GND #PWR0102
U 1 1 60D217BD
P 4700 1425
F 0 "#PWR0102" H 4700 1175 50  0001 C CNN
F 1 "GND" V 4705 1297 50  0000 R CNN
F 2 "" H 4700 1425 50  0001 C CNN
F 3 "" H 4700 1425 50  0001 C CNN
	1    4700 1425
	0    -1   -1   0   
$EndComp
Text GLabel 4700 1825 2    50   Input ~ 0
Vin
Text GLabel 4700 2525 2    50   Input ~ 0
LCD_CS
Text GLabel 4700 2425 2    50   Input ~ 0
LCD_A0
Text GLabel 4700 2225 2    50   Input ~ 0
MOSI
Text GLabel 4700 2125 2    50   Input ~ 0
Beep
Text GLabel 4700 2025 2    50   Input ~ 0
LCD_BL
Text GLabel 4700 1925 2    50   Input ~ 0
EN
Text GLabel 4700 1225 2    50   Input ~ 0
3.3V
Text GLabel 4700 1125 2    50   Input ~ 0
BTN
Text GLabel 4700 1025 2    50   Input ~ 0
Vbat
$Comp
L Connector:Conn_01x16_Male J6
U 1 1 60D13074
P 4425 1725
F 0 "J6" H 4533 2514 50  0000 C CNN
F 1 "Conn_01x16_Male" H 4533 2515 50  0001 C CNN
F 2 "Footprints:PinHeader_1x16_P2.54mm_Vertical" H 4425 1725 50  0001 C CNN
F 3 "~" H 4425 1725 50  0001 C CNN
	1    4425 1725
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 9775 1625 9775
Wire Wire Line
	1625 9975 1650 9975
Wire Wire Line
	1625 9775 1625 9975
$Comp
L Components:AC-DC PS1
U 1 1 60FF32A6
P 2050 10075
F 0 "PS1" H 2050 10400 50  0000 C CNN
F 1 "AC-DC" H 2050 10309 50  0000 C CNN
F 2 "Footprints:AC-DC_Aliexpress" H 2050 9775 50  0001 C CNN
F 3 "http://www.hlktech.net/product_detail.php?ProId=54" H 2450 9725 50  0001 C CNN
	1    2050 10075
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 9775 1175 9775
Wire Wire Line
	1150 10175 1500 10175
Wire Wire Line
	1150 9975 1150 9775
Wire Wire Line
	1125 9975 1150 9975
Connection ~ 1500 10175
Wire Wire Line
	1650 10175 1500 10175
Connection ~ 1500 9775
Wire Wire Line
	1500 9775 1500 9825
Wire Wire Line
	1475 9775 1500 9775
Wire Wire Line
	1500 10175 1500 10125
Wire Wire Line
	1150 10075 1150 10175
Wire Wire Line
	1125 10075 1150 10075
$Comp
L Device:Fuse F1
U 1 1 653D1796
P 1325 9775
F 0 "F1" V 1400 9725 50  0000 C CNN
F 1 "Fuse" V 1219 9775 50  0001 C CNN
F 2 "Footprints:Fuse_Littelfuse_395Series" V 1255 9775 50  0001 C CNN
F 3 "~" H 1325 9775 50  0001 C CNN
	1    1325 9775
	0    1    1    0   
$EndComp
$Comp
L Components:Conn_01x02 P5
U 1 1 6519880B
P 925 9975
F 0 "P5" H 925 10225 50  0000 C CNN
F 1 "3.81x4 terminal block" V 1025 9975 50  0001 C CNN
F 2 "Footprints:PhoenixContact_MC_1,5_2-G-3.81_1x02_P3.81mm_Horizontal" H 925 9975 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Pluggable-System-Terminal-Block_Ningbo-Kangnex-Elec-WJ15EDGRC-3-81-4P_C7245.html" H 925 9975 50  0001 C CNN
	1    925  9975
	-1   0    0    1   
$EndComp
$Comp
L 3Phase-rescue:Varistor-3Phase-rescue RV1
U 1 1 5BDB8784
P 1500 9975
F 0 "RV1" V 1625 9975 50  0000 C CNN
F 1 "05D391K" H 1375 9975 50  0001 C CNN
F 2 "Footprints:RV_Disc_D7mm_W3.4mm_P5mm" V 1430 9975 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Varistors_VDR-Varistor-VDR-05D391K_C283919.html" H 1500 9975 50  0001 C CNN
	1    1500 9975
	-1   0    0    1   
$EndComp
$Comp
L Device:Crystal Y1
U 1 1 61252A77
P 6025 4000
F 0 "Y1" H 6025 4175 50  0000 C CNN
F 1 "32.768kHz" H 6025 4267 50  0001 C CNN
F 2 "Crystal:Crystal_SMD_3215-2Pin_3.2x1.5mm" H 6025 4176 50  0001 C CNN
F 3 "~" H 6025 4000 50  0001 C CNN
	1    6025 4000
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C28
U 1 1 615261BA
P 6025 3675
F 0 "C28" V 6075 3500 50  0000 L CNN
F 1 "100nF 25v" H 6035 3595 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 6025 3675 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 6025 3675 50  0001 C CNN
	1    6025 3675
	0    1    -1   0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C26
U 1 1 61645885
P 5875 3825
F 0 "C26" V 5975 3750 50  0000 L CNN
F 1 "100nF 25v" H 5885 3745 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 5875 3825 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 5875 3825 50  0001 C CNN
	1    5875 3825
	0    1    -1   0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C27
U 1 1 61645C12
P 5875 4175
F 0 "C27" V 5775 4100 50  0000 L CNN
F 1 "100nF 25v" H 5885 4095 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 5875 4175 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 5875 4175 50  0001 C CNN
	1    5875 4175
	0    1    -1   0   
$EndComp
Wire Wire Line
	6025 4150 6025 4175
Connection ~ 6025 4175
Wire Wire Line
	6025 4175 5975 4175
Wire Wire Line
	6025 3850 6025 3825
Connection ~ 6025 3825
Wire Wire Line
	6025 3825 5975 3825
$Comp
L 3Phase-rescue:R-3Phase-rescue R35
U 1 1 619CBE54
P 3650 3975
F 0 "R35" V 3730 3975 50  0000 C CNN
F 1 "100h" V 3650 3975 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 3580 3975 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_100R-100R-1_C125923.html" H 3650 3975 50  0001 C CNN
	1    3650 3975
	0    -1   1    0   
$EndComp
Text GLabel 3875 3975 2    50   Input ~ 0
IN_4N
Wire Wire Line
	3800 3975 3850 3975
Connection ~ 3850 3975
Wire Wire Line
	3850 3975 3875 3975
Wire Wire Line
	3850 3950 3850 3975
Wire Wire Line
	3850 3725 3875 3725
$Comp
L Components:MCP1811 U9
U 1 1 62149434
P 5575 4675
F 0 "U9" H 5375 4925 50  0000 C CNN
F 1 "MCP1811" H 5575 4951 50  0001 C CNN
F 2 "Footprints:SOT-23-5" H 5325 5025 50  0001 L CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21813F.pdf" H 5575 4575 50  0001 C CNN
	1    5575 4675
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C34
U 1 1 621EA448
P 6025 4850
F 0 "C34" V 6075 4675 50  0000 L CNN
F 1 "100nF 25v" H 6035 4770 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 6025 4850 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 6025 4850 50  0001 C CNN
	1    6025 4850
	1    0    0    1   
$EndComp
Wire Wire Line
	5975 4575 6025 4575
Wire Wire Line
	6025 4750 6025 4575
Connection ~ 6025 4575
Wire Wire Line
	5575 4975 5575 5000
Wire Wire Line
	6025 5000 6025 4950
Text GLabel 4950 4575 0    50   Input ~ 0
Vbat
$Comp
L 3Phase-rescue:R-3Phase-rescue R38
U 1 1 622D00A6
P 4975 4950
F 0 "R38" V 5055 4950 50  0000 C CNN
F 1 "2.2k" V 4975 4950 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 4905 4950 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 4975 4950 50  0001 C CNN
	1    4975 4950
	-1   0    0    1   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C35
U 1 1 6231C05A
P 5200 5175
F 0 "C35" H 5225 5100 50  0000 L CNN
F 1 "100nF 25v" H 5210 5095 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 5200 5175 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 5200 5175 50  0001 C CNN
	1    5200 5175
	1    0    0    1   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C30
U 1 1 6231EA5D
P 5075 4800
F 0 "C30" V 5175 4825 50  0000 L CNN
F 1 "100nF 25v" H 5085 4720 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 5075 4800 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 5075 4800 50  0001 C CNN
	1    5075 4800
	1    0    0    1   
$EndComp
Wire Wire Line
	4950 4575 4975 4575
Wire Wire Line
	5075 4700 5075 4675
Connection ~ 5075 4575
Wire Wire Line
	5075 4575 5175 4575
Wire Wire Line
	5075 4675 5175 4675
Connection ~ 5075 4675
Wire Wire Line
	5075 4675 5075 4575
Wire Wire Line
	5075 4900 5075 5000
Wire Wire Line
	5075 5000 5200 5000
Connection ~ 5575 5000
$Comp
L power:GND #PWR0105
U 1 1 6244BE57
P 6075 5000
F 0 "#PWR0105" H 6075 4750 50  0001 C CNN
F 1 "GND" H 6075 4850 50  0000 C CNN
F 2 "" H 6075 5000 50  0000 C CNN
F 3 "" H 6075 5000 50  0000 C CNN
	1    6075 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4975 4800 4975 4575
Connection ~ 4975 4575
Wire Wire Line
	4975 4575 5075 4575
Wire Wire Line
	4975 5100 4975 5350
Wire Wire Line
	4975 5350 5075 5350
Text GLabel 4950 4375 0    50   Input ~ 0
BTN
$Comp
L power:GND #PWR0106
U 1 1 62D62961
P 5725 4175
F 0 "#PWR0106" H 5725 3925 50  0001 C CNN
F 1 "GND" V 5825 4100 50  0000 C CNN
F 2 "" H 5725 4175 50  0000 C CNN
F 3 "" H 5725 4175 50  0000 C CNN
	1    5725 4175
	0    1    1    0   
$EndComp
Wire Wire Line
	5725 4175 5750 4175
Wire Wire Line
	5750 4175 5750 3825
Wire Wire Line
	5750 3825 5775 3825
Connection ~ 5750 4175
Wire Wire Line
	5750 4175 5775 4175
Wire Wire Line
	5750 3825 5750 3675
Wire Wire Line
	5750 3675 5925 3675
Connection ~ 5750 3825
Wire Wire Line
	6150 9775 6175 9775
Wire Wire Line
	5675 10100 5900 10100
$Comp
L power:GND #PWR0109
U 1 1 62EF0395
P 5900 10100
F 0 "#PWR0109" H 5900 9850 50  0001 C CNN
F 1 "GND" V 5905 9972 50  0000 R CNN
F 2 "" H 5900 10100 50  0001 C CNN
F 3 "" H 5900 10100 50  0001 C CNN
	1    5900 10100
	0    -1   1    0   
$EndComp
Wire Wire Line
	5850 9900 5675 9900
$Comp
L Components:SSR_MOSFET_DIP4 U11
U 1 1 699206B6
P 5375 10000
F 0 "U11" H 5100 10025 50  0000 C CNN
F 1 "SSR_MOSFET_DIP4" H 5375 9774 50  0001 C CNN
F 2 "Footprints:DIP-4_W7.62mm" H 5175 9800 50  0001 L CIN
F 3 "https://docs.broadcom.com/docs/AV02-0173EN" H 5375 10000 50  0001 L CNN
	1    5375 10000
	-1   0    0    -1  
$EndComp
$Comp
L Components:Conn_01x02 P6
U 1 1 6993C0AD
P 4800 9950
F 0 "P6" H 4800 10200 50  0000 C CNN
F 1 "3.81x4 terminal block" V 4900 9950 50  0001 C CNN
F 2 "Footprints:PhoenixContact_MC_1,5_2-G-3.81_1x02_P3.81mm_Horizontal" H 4800 9950 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Pluggable-System-Terminal-Block_Ningbo-Kangnex-Elec-WJ15EDGRC-3-81-4P_C7245.html" H 4800 9950 50  0001 C CNN
	1    4800 9950
	-1   0    0    1   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R57
U 1 1 61F8F42D
P 6000 9900
F 0 "R57" V 5900 9900 50  0000 C CNN
F 1 "2.2k" V 6000 9900 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5930 9900 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 6000 9900 50  0001 C CNN
	1    6000 9900
	0    1    -1   0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C32
U 1 1 63C8C6E4
P 8075 7825
F 0 "C32" V 8125 7875 50  0000 L CNN
F 1 "100nF 25v" H 8085 7745 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 8075 7825 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 8075 7825 50  0001 C CNN
	1    8075 7825
	-1   0    0    1   
$EndComp
Wire Wire Line
	8075 7950 8075 7925
$Comp
L Components:REF2025 U8
U 1 1 63D9481C
P 8500 7825
F 0 "U8" H 8325 7650 50  0000 C CNN
F 1 "REF2025" H 8375 7550 50  0001 C CIN
F 2 "Footprints:SOT-23-5" H 8575 7450 50  0001 L CIN
F 3 "http://www.ti.com/lit/ds/symlink/ref3240-ep.pdf" H 8500 7875 50  0001 C CIN
	1    8500 7825
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8075 7950 8100 7950
Wire Wire Line
	8100 7700 8075 7700
Wire Wire Line
	8075 7725 8075 7700
Text GLabel 9175 7700 2    50   Input ~ 0
1.25V
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C29
U 1 1 643D8EA9
P 9025 7950
F 0 "C29" V 8925 7875 50  0000 L CNN
F 1 "100nF 25v" H 9035 7870 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 9025 7950 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 9025 7950 50  0001 C CNN
	1    9025 7950
	-1   0    0    1   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C31
U 1 1 643D94FF
P 8825 7950
F 0 "C31" V 8725 7875 50  0000 L CNN
F 1 "100nF 25v" H 8835 7870 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 8825 7950 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 8825 7950 50  0001 C CNN
	1    8825 7950
	-1   0    0    1   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C36
U 1 1 63C8D5A9
P 7900 7825
F 0 "C36" V 7950 7875 50  0000 L CNN
F 1 "100nF 25v" H 7910 7745 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 7900 7825 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 7900 7825 50  0001 C CNN
	1    7900 7825
	-1   0    0    1   
$EndComp
Wire Wire Line
	9175 7700 9025 7700
Wire Wire Line
	9025 7850 9025 7700
Connection ~ 9025 7700
Wire Wire Line
	9025 7700 8700 7700
Text GLabel 9175 7825 2    50   Input ~ 0
3.3Va
Wire Wire Line
	9175 7825 8825 7825
Wire Wire Line
	8825 7850 8825 7825
Connection ~ 8825 7825
Wire Wire Line
	8700 7950 8725 7950
Wire Wire Line
	8725 7950 8725 7825
Connection ~ 8725 7825
Wire Wire Line
	8725 7825 8700 7825
$Comp
L power:GND #PWR0111
U 1 1 64B04F25
P 9175 8075
F 0 "#PWR0111" H 9175 7825 50  0001 C CNN
F 1 "GND" V 9180 7947 50  0000 R CNN
F 2 "" H 9175 8075 50  0001 C CNN
F 3 "" H 9175 8075 50  0001 C CNN
	1    9175 8075
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9175 8075 9025 8075
Wire Wire Line
	9025 8075 9025 8050
Wire Wire Line
	9025 8075 8825 8075
Wire Wire Line
	8825 8075 8825 8050
Connection ~ 9025 8075
Wire Wire Line
	8825 8075 8075 8075
Connection ~ 8825 8075
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C33
U 1 1 64D27015
P 8125 8650
F 0 "C33" V 8000 8575 50  0000 L CNN
F 1 "100nF 25v" H 8135 8570 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 8125 8650 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 8125 8650 50  0001 C CNN
	1    8125 8650
	-1   0    0    1   
$EndComp
$Comp
L Components:PGA281 U1
U 1 1 60E8F881
P 2250 3775
F 0 "U1" H 2225 4100 50  0000 C CNN
F 1 "PGA281" H 2412 4076 50  0001 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 2125 3100 50  0001 L CIN
F 3 "" H 2395 3780 50  0001 L CNN
	1    2250 3775
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 3725 2075 3725
Wire Wire Line
	1475 4375 1900 4375
Wire Wire Line
	1200 3575 1300 3575
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C12
U 1 1 6169C6B1
P 1900 4250
F 0 "C12" H 1975 4250 50  0000 L CNN
F 1 "100nF 25v" H 1910 4170 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 1900 4250 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 1900 4250 50  0001 C CNN
	1    1900 4250
	-1   0    0    1   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C17
U 1 1 6169CFA0
P 2050 4250
F 0 "C17" H 1975 4425 50  0000 L CNN
F 1 "100nF 25v" H 2060 4170 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 2050 4250 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 2050 4250 50  0001 C CNN
	1    2050 4250
	-1   0    0    1   
$EndComp
Wire Wire Line
	1900 4150 1900 3575
Wire Wire Line
	1900 3575 2075 3575
Wire Wire Line
	2075 4125 2050 4125
Wire Wire Line
	2050 4125 2050 4150
Wire Wire Line
	2050 4350 2050 4375
Wire Wire Line
	2050 4375 1900 4375
Wire Wire Line
	1900 4350 1900 4375
Connection ~ 1900 4375
Text GLabel 1850 3575 0    50   Input ~ 0
12V
Wire Wire Line
	1850 3575 1900 3575
Connection ~ 1900 3575
Text GLabel 1850 4125 0    50   Input ~ 0
-12V
Wire Wire Line
	1850 4125 2050 4125
Connection ~ 2050 4125
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C39
U 1 1 622A695B
P 2050 1300
F 0 "C39" V 2100 1350 50  0000 L CNN
F 1 "100nF 10v" H 2060 1220 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 2050 1300 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 2050 1300 50  0001 C CNN
	1    2050 1300
	0    1    1    0   
$EndComp
Wire Wire Line
	1775 1100 1775 1300
Wire Wire Line
	2250 800  2250 1300
Wire Wire Line
	1100 650  1125 650 
$Comp
L 3Phase-rescue:R-3Phase-rescue R24
U 1 1 5BD04E02
P 1275 650
F 0 "R24" V 1355 650 50  0000 C CNN
F 1 "499k" V 1275 650 50  0001 C CNN
F 2 "Footprints:R_1206_3216Metric" V 1205 650 50  0001 C CNN
F 3 "" H 1275 650 50  0001 C CNN
F 4 "C517514" V 1275 650 50  0001 C CNN "LCSC"
	1    1275 650 
	0    1    1    0   
$EndComp
Wire Wire Line
	1425 650  1450 650 
$Comp
L 3Phase-rescue:R-3Phase-rescue R25
U 1 1 5BD04F42
P 1600 650
F 0 "R25" V 1680 650 50  0000 C CNN
F 1 "499k" V 1600 650 50  0001 C CNN
F 2 "Footprints:R_1206_3216Metric" V 1530 650 50  0001 C CNN
F 3 "" H 1600 650 50  0001 C CNN
F 4 "C517514" V 1600 650 50  0001 C CNN "LCSC"
	1    1600 650 
	0    1    1    0   
$EndComp
Wire Wire Line
	2325 650  2325 900 
Connection ~ 2125 650 
Wire Wire Line
	2125 650  2325 650 
Wire Wire Line
	1750 650  1775 650 
Connection ~ 1775 650 
Wire Wire Line
	1775 650  1800 650 
Wire Wire Line
	2100 650  2125 650 
$Comp
L 3Phase-rescue:R-3Phase-rescue R61
U 1 1 60C90D6D
P 1950 650
F 0 "R61" V 2030 650 50  0000 C CNN
F 1 "12" V 1950 650 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1880 650 50  0001 C CNN
F 3 "" H 1950 650 50  0001 C CNN
	1    1950 650 
	0    1    1    0   
$EndComp
Wire Wire Line
	2125 650  2125 725 
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C42
U 1 1 60C963B4
P 2125 825
F 0 "C42" H 1900 825 50  0000 L CNN
F 1 "330pF NP0" H 2135 745 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 2125 825 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 2125 825 50  0001 C CNN
	1    2125 825 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1775 675  1775 650 
$Comp
L 3Phase-rescue:R-3Phase-rescue R31
U 1 1 5BD051BF
P 1775 825
F 0 "R31" V 1855 825 50  0000 C CNN
F 1 "1k02" V 1775 825 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1705 825 50  0001 C CNN
F 3 "" H 1775 825 50  0001 C CNN
F 4 "C705734" V 1775 825 50  0001 C CNN "LCSC"
	1    1775 825 
	-1   0    0    1   
$EndComp
Wire Wire Line
	1100 650  1100 900 
Wire Wire Line
	1075 900  1100 900 
$Comp
L Components:Conn_01x02 P1
U 1 1 63AF2892
P 875 900
F 0 "P1" H 875 1150 50  0000 C CNN
F 1 "3.81x4 terminal block" V 975 900 50  0001 C CNN
F 2 "Footprints:PhoenixContact_MC_1,5_2-G-3.81_1x02_P3.81mm_Horizontal" H 875 900 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Pluggable-System-Terminal-Block_Ningbo-Kangnex-Elec-WJ15EDGRC-3-81-4P_C7245.html" H 875 900 50  0001 C CNN
	1    875  900 
	-1   0    0    1   
$EndComp
Connection ~ 1775 1100
Wire Wire Line
	1775 1000 1075 1000
Wire Wire Line
	1775 1000 1775 1100
Connection ~ 1775 1000
Wire Wire Line
	1775 975  1775 1000
Wire Wire Line
	1800 1000 1775 1000
Wire Wire Line
	2125 1000 2125 925 
Wire Wire Line
	1775 1100 2375 1100
Wire Wire Line
	2125 1000 2375 1000
Connection ~ 2125 1000
Wire Wire Line
	2100 1000 2125 1000
$Comp
L 3Phase-rescue:R-3Phase-rescue R67
U 1 1 60C91A12
P 1950 1000
F 0 "R67" V 1875 1000 50  0000 C CNN
F 1 "12" V 1950 1000 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1880 1000 50  0001 C CNN
F 3 "" H 1950 1000 50  0001 C CNN
	1    1950 1000
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C41
U 1 1 628BED6E
P 3200 4250
F 0 "C41" H 3210 4320 50  0000 L CNN
F 1 "100nF 25v" H 3210 4170 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3200 4250 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3200 4250 50  0001 C CNN
	1    3200 4250
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 628C9DC5
P 3200 4625
F 0 "#PWR0113" H 3200 4375 50  0001 C CNN
F 1 "GND" H 3200 4475 50  0000 C CNN
F 2 "" H 3200 4625 50  0000 C CNN
F 3 "" H 3200 4625 50  0000 C CNN
	1    3200 4625
	1    0    0    -1  
$EndComp
Wire Wire Line
	3075 4125 3100 4125
Wire Wire Line
	3100 4125 3100 4375
Wire Wire Line
	3075 3575 3200 3575
Wire Wire Line
	3200 3575 3200 4150
Wire Wire Line
	3075 3850 3350 3850
Wire Wire Line
	3350 3850 3350 4150
Wire Wire Line
	3075 3725 3500 3725
Wire Wire Line
	3500 3975 3075 3975
Wire Wire Line
	3100 4375 3200 4375
Wire Wire Line
	3200 4375 3200 4350
Wire Wire Line
	3200 4375 3350 4375
Wire Wire Line
	3350 4375 3350 4350
Connection ~ 3200 4375
Wire Wire Line
	3375 3850 3350 3850
Connection ~ 3350 3850
Text GLabel 3375 3575 2    50   Input ~ 0
3.3Va
Wire Wire Line
	3375 3575 3200 3575
Connection ~ 3200 3575
Text GLabel 2875 4575 3    50   Input ~ 0
3.3V
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C40
U 1 1 62E87E23
P 3050 4550
F 0 "C40" H 3060 4620 50  0000 L CNN
F 1 "100nF 25v" H 3060 4470 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3050 4550 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3050 4550 50  0001 C CNN
	1    3050 4550
	0    1    -1   0   
$EndComp
Wire Wire Line
	3200 4375 3200 4550
Wire Wire Line
	3150 4550 3200 4550
Connection ~ 3200 4550
Wire Wire Line
	3200 4550 3200 4625
Wire Wire Line
	2875 4575 2875 4550
Wire Wire Line
	2950 4550 2875 4550
Connection ~ 2875 4550
Wire Wire Line
	2875 4550 2875 4525
Text GLabel 2775 4575 3    50   Input ~ 0
IN4_G0
Text GLabel 2675 4575 3    50   Input ~ 0
IN4_G1
Text GLabel 2575 4575 3    50   Input ~ 0
IN4_G2
Text GLabel 2475 4575 3    50   Input ~ 0
IN4_G3
Text GLabel 2375 4575 3    50   Input ~ 0
IN4_G4
Wire Wire Line
	2375 4575 2375 4525
Wire Wire Line
	2475 4575 2475 4525
Wire Wire Line
	2575 4575 2575 4525
Wire Wire Line
	2675 4575 2675 4525
Wire Wire Line
	2775 4575 2775 4525
Wire Wire Line
	2275 4575 2275 4550
$Comp
L Device:LED_Small_ALT D2
U 1 1 6330473F
P 1650 4550
F 0 "D2" H 1550 4600 50  0000 C CNN
F 1 "LED_Small_ALT" H 1650 4434 50  0001 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" V 1650 4550 50  0001 C CNN
F 3 "~" V 1650 4550 50  0001 C CNN
	1    1650 4550
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R52
U 1 1 63305208
P 1950 4550
F 0 "R52" V 1900 4400 50  0000 C CNN
F 1 "2.2k" V 1950 4550 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1880 4550 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1950 4550 50  0001 C CNN
	1    1950 4550
	0    1    -1   0   
$EndComp
Wire Wire Line
	2100 4550 2275 4550
Connection ~ 2275 4550
Wire Wire Line
	2275 4550 2275 4525
Wire Wire Line
	1800 4550 1750 4550
Wire Wire Line
	1475 4575 1475 4550
Wire Wire Line
	1550 4550 1475 4550
Connection ~ 1475 4550
Wire Wire Line
	1475 4550 1475 4375
$Comp
L Components:NanoPi U10
U 1 1 635DCC5C
P 8225 6800
F 0 "U10" H 8275 7000 50  0000 L CNN
F 1 "NanoPi" H 8575 7000 50  0000 L CNN
F 2 "Footprints:NanoPI_Air" H 8225 6800 50  0001 C CNN
F 3 "" H 8225 6800 50  0001 C CNN
	1    8225 6800
	1    0    0    -1  
$EndComp
Text GLabel 2275 4575 3    50   Input ~ 0
IN4_ERR
$Comp
L 3Phase-rescue:R-3Phase-rescue R17
U 1 1 636AC9C6
P 3650 5275
F 0 "R17" V 3575 5275 50  0000 C CNN
F 1 "100h" V 3650 5275 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 3580 5275 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_100R-100R-1_C125923.html" H 3650 5275 50  0001 C CNN
	1    3650 5275
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C50
U 1 1 636AD102
P 3850 5400
F 0 "C50" H 3625 5400 50  0000 L CNN
F 1 "100nF 25v" H 3860 5320 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3850 5400 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3850 5400 50  0001 C CNN
	1    3850 5400
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:CONN_01X04-3Phase-rescue J2
U 1 1 636AD10C
P 875 5475
F 0 "J2" H 875 5725 50  0000 C CNN
F 1 "95001-6P6C" H 675 5825 50  0001 C CNN
F 2 "Footprints:KF2EDGR_2.54x4" H 875 5475 50  0001 C CNN
F 3 "" H 875 5475 50  0001 C CNN
	1    875  5475
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R1
U 1 1 636AD116
P 1300 5700
F 0 "R1" V 1380 5700 50  0000 C CNN
F 1 "2.2k" V 1300 5700 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1230 5700 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1300 5700 50  0001 C CNN
	1    1300 5700
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R4
U 1 1 636AD120
P 1475 5700
F 0 "R4" V 1555 5700 50  0000 C CNN
F 1 "2.2k" V 1475 5700 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1405 5700 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1475 5700 50  0001 C CNN
	1    1475 5700
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R7
U 1 1 636AD12A
P 1650 5275
F 0 "R7" V 1730 5275 50  0000 C CNN
F 1 "2.2k" V 1650 5275 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1580 5275 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1650 5275 50  0001 C CNN
	1    1650 5275
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R8
U 1 1 636AD134
P 1650 5525
F 0 "R8" V 1575 5525 50  0000 C CNN
F 1 "2.2k" V 1650 5525 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1580 5525 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1650 5525 50  0001 C CNN
	1    1650 5525
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C13
U 1 1 636AD13E
P 2050 5400
F 0 "C13" H 2125 5400 50  0000 L CNN
F 1 "100nF 25v" H 2060 5320 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 2050 5400 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 2050 5400 50  0001 C CNN
	1    2050 5400
	-1   0    0    1   
$EndComp
Wire Wire Line
	1800 5275 2050 5275
Wire Wire Line
	2050 5300 2050 5275
Wire Wire Line
	1800 5525 2050 5525
Wire Wire Line
	2050 5500 2050 5525
Wire Wire Line
	2050 5525 2075 5525
Wire Wire Line
	1475 5550 1475 5525
Wire Wire Line
	1500 5525 1475 5525
Wire Wire Line
	1300 5550 1300 5425
Wire Wire Line
	1300 5275 1500 5275
Wire Wire Line
	1075 5625 1200 5625
Wire Wire Line
	1200 5625 1200 5925
Wire Wire Line
	1200 5925 1300 5925
Wire Wire Line
	1300 5850 1300 5925
Connection ~ 1300 5925
Wire Wire Line
	1300 5925 1475 5925
Wire Wire Line
	1475 5925 1475 5850
Wire Wire Line
	1075 5525 1475 5525
Connection ~ 1475 5525
Wire Wire Line
	1075 5425 1300 5425
Connection ~ 1300 5425
Wire Wire Line
	1300 5425 1300 5275
Wire Wire Line
	1075 5325 1200 5325
Wire Wire Line
	1200 5325 1200 5125
Text GLabel 3875 5275 2    50   Input ~ 0
IN_5P
Wire Wire Line
	3800 5275 3850 5275
Wire Wire Line
	3850 5300 3850 5275
Connection ~ 3850 5275
Text GLabel 3375 5400 2    50   Input ~ 0
1.25V
Connection ~ 2050 5275
Connection ~ 2050 5525
Connection ~ 1475 5925
$Comp
L power:GND #PWR0101
U 1 1 636AD167
P 1475 6125
F 0 "#PWR0101" H 1475 5875 50  0001 C CNN
F 1 "GND" H 1475 5975 50  0000 C CNN
F 2 "" H 1475 6125 50  0000 C CNN
F 3 "" H 1475 6125 50  0000 C CNN
	1    1475 6125
	1    0    0    -1  
$EndComp
Text GLabel 1300 5125 2    50   Input ~ 0
5V
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C47
U 1 1 636AD172
P 3350 5800
F 0 "C47" V 3300 5850 50  0000 L CNN
F 1 "100nF 25v" H 3360 5720 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3350 5800 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3350 5800 50  0001 C CNN
	1    3350 5800
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R19
U 1 1 636AD17C
P 3650 5525
F 0 "R19" V 3730 5525 50  0000 C CNN
F 1 "100h" V 3650 5525 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 3580 5525 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_100R-100R-1_C125923.html" H 3650 5525 50  0001 C CNN
	1    3650 5525
	0    -1   1    0   
$EndComp
Text GLabel 3875 5525 2    50   Input ~ 0
IN_5N
Wire Wire Line
	3800 5525 3850 5525
Connection ~ 3850 5525
Wire Wire Line
	3850 5525 3875 5525
Wire Wire Line
	3850 5500 3850 5525
Wire Wire Line
	3850 5275 3875 5275
$Comp
L Components:PGA281 U2
U 1 1 636AD18C
P 2250 5325
F 0 "U2" H 2225 5650 50  0000 C CNN
F 1 "PGA281" H 2412 5626 50  0001 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 2125 4650 50  0001 L CIN
F 3 "" H 2395 5330 50  0001 L CNN
	1    2250 5325
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 5275 2075 5275
Wire Wire Line
	1475 5925 1900 5925
Wire Wire Line
	1200 5125 1300 5125
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C1
U 1 1 636AD199
P 1900 5800
F 0 "C1" H 1975 5800 50  0000 L CNN
F 1 "100nF 25v" H 1910 5720 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 1900 5800 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 1900 5800 50  0001 C CNN
	1    1900 5800
	-1   0    0    1   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C14
U 1 1 636AD1A3
P 2050 5800
F 0 "C14" H 1975 5975 50  0000 L CNN
F 1 "100nF 25v" H 2060 5720 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 2050 5800 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 2050 5800 50  0001 C CNN
	1    2050 5800
	-1   0    0    1   
$EndComp
Wire Wire Line
	1900 5700 1900 5125
Wire Wire Line
	1900 5125 2075 5125
Wire Wire Line
	2075 5675 2050 5675
Wire Wire Line
	2050 5675 2050 5700
Wire Wire Line
	2050 5900 2050 5925
Wire Wire Line
	2050 5925 1900 5925
Wire Wire Line
	1900 5900 1900 5925
Connection ~ 1900 5925
Text GLabel 1850 5125 0    50   Input ~ 0
12V
Wire Wire Line
	1850 5125 1900 5125
Connection ~ 1900 5125
Text GLabel 1850 5675 0    50   Input ~ 0
-12V
Wire Wire Line
	1850 5675 2050 5675
Connection ~ 2050 5675
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C23
U 1 1 636AD1BB
P 3200 5800
F 0 "C23" V 3150 5850 50  0000 L CNN
F 1 "100nF 25v" H 3210 5720 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3200 5800 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3200 5800 50  0001 C CNN
	1    3200 5800
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 636AD1C5
P 3200 6175
F 0 "#PWR0103" H 3200 5925 50  0001 C CNN
F 1 "GND" H 3200 6025 50  0000 C CNN
F 2 "" H 3200 6175 50  0000 C CNN
F 3 "" H 3200 6175 50  0000 C CNN
	1    3200 6175
	1    0    0    -1  
$EndComp
Wire Wire Line
	3075 5675 3100 5675
Wire Wire Line
	3100 5675 3100 5925
Wire Wire Line
	3075 5125 3200 5125
Wire Wire Line
	3200 5125 3200 5700
Wire Wire Line
	3075 5400 3350 5400
Wire Wire Line
	3350 5400 3350 5700
Wire Wire Line
	3075 5275 3500 5275
Wire Wire Line
	3500 5525 3075 5525
Wire Wire Line
	3100 5925 3200 5925
Wire Wire Line
	3200 5925 3200 5900
Wire Wire Line
	3200 5925 3350 5925
Wire Wire Line
	3350 5925 3350 5900
Connection ~ 3200 5925
Wire Wire Line
	3375 5400 3350 5400
Connection ~ 3350 5400
Text GLabel 3375 5125 2    50   Input ~ 0
3.3Va
Wire Wire Line
	3375 5125 3200 5125
Connection ~ 3200 5125
Text GLabel 2875 6125 3    50   Input ~ 0
3.3V
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C20
U 1 1 636AD1E2
P 3050 6100
F 0 "C20" V 2925 6025 50  0000 L CNN
F 1 "100nF 25v" H 3060 6020 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3050 6100 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3050 6100 50  0001 C CNN
	1    3050 6100
	0    1    -1   0   
$EndComp
Wire Wire Line
	3200 5925 3200 6100
Wire Wire Line
	3150 6100 3200 6100
Connection ~ 3200 6100
Wire Wire Line
	3200 6100 3200 6175
Wire Wire Line
	2875 6125 2875 6100
Wire Wire Line
	2950 6100 2875 6100
Connection ~ 2875 6100
Wire Wire Line
	2875 6100 2875 6075
Text GLabel 2775 6125 3    50   Input ~ 0
IN5_G0
Text GLabel 2575 6125 3    50   Input ~ 0
IN5_G2
Text GLabel 2475 6125 3    50   Input ~ 0
IN5_G3
Text GLabel 2375 6125 3    50   Input ~ 0
IN5_G4
Wire Wire Line
	2375 6125 2375 6075
Wire Wire Line
	2475 6125 2475 6075
Wire Wire Line
	2575 6125 2575 6075
Wire Wire Line
	2675 6125 2675 6075
Wire Wire Line
	2775 6125 2775 6075
Wire Wire Line
	2275 6125 2275 6100
$Comp
L Device:LED_Small_ALT D3
U 1 1 636AD1FF
P 1650 6100
F 0 "D3" H 1550 6150 50  0000 C CNN
F 1 "LED_Small_ALT" H 1650 5984 50  0001 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" V 1650 6100 50  0001 C CNN
F 3 "~" V 1650 6100 50  0001 C CNN
	1    1650 6100
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R13
U 1 1 636AD209
P 1950 6100
F 0 "R13" V 1850 6100 50  0000 C CNN
F 1 "2.2k" V 1950 6100 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1880 6100 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1950 6100 50  0001 C CNN
	1    1950 6100
	0    1    -1   0   
$EndComp
Wire Wire Line
	2100 6100 2275 6100
Connection ~ 2275 6100
Wire Wire Line
	2275 6100 2275 6075
Wire Wire Line
	1800 6100 1750 6100
Wire Wire Line
	1475 6125 1475 6100
Wire Wire Line
	1550 6100 1475 6100
Connection ~ 1475 6100
Wire Wire Line
	1475 6100 1475 5925
Text GLabel 2275 6125 3    50   Input ~ 0
IN5_ERR
$Comp
L 3Phase-rescue:R-3Phase-rescue R20
U 1 1 63702B91
P 3650 6825
F 0 "R20" V 3575 6825 50  0000 C CNN
F 1 "100h" V 3650 6825 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 3580 6825 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_100R-100R-1_C125923.html" H 3650 6825 50  0001 C CNN
	1    3650 6825
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C51
U 1 1 63703445
P 3850 6950
F 0 "C51" H 3625 6950 50  0000 L CNN
F 1 "100nF 25v" H 3860 6870 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3850 6950 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3850 6950 50  0001 C CNN
	1    3850 6950
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:CONN_01X04-3Phase-rescue J3
U 1 1 6370344F
P 875 7025
F 0 "J3" H 875 7275 50  0000 C CNN
F 1 "95001-6P6C" H 675 7375 50  0001 C CNN
F 2 "Footprints:KF2EDGR_2.54x4" H 875 7025 50  0001 C CNN
F 3 "" H 875 7025 50  0001 C CNN
	1    875  7025
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R2
U 1 1 63703459
P 1300 7250
F 0 "R2" V 1380 7250 50  0000 C CNN
F 1 "2.2k" V 1300 7250 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1230 7250 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1300 7250 50  0001 C CNN
	1    1300 7250
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R5
U 1 1 63703463
P 1475 7250
F 0 "R5" V 1555 7250 50  0000 C CNN
F 1 "2.2k" V 1475 7250 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1405 7250 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1475 7250 50  0001 C CNN
	1    1475 7250
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R9
U 1 1 6370346D
P 1650 6825
F 0 "R9" V 1730 6825 50  0000 C CNN
F 1 "2.2k" V 1650 6825 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1580 6825 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1650 6825 50  0001 C CNN
	1    1650 6825
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R10
U 1 1 63703477
P 1650 7075
F 0 "R10" V 1575 7075 50  0000 C CNN
F 1 "2.2k" V 1650 7075 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1580 7075 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1650 7075 50  0001 C CNN
	1    1650 7075
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C15
U 1 1 63703481
P 2050 6950
F 0 "C15" H 2125 6950 50  0000 L CNN
F 1 "100nF 25v" H 2060 6870 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 2050 6950 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 2050 6950 50  0001 C CNN
	1    2050 6950
	-1   0    0    1   
$EndComp
Wire Wire Line
	1800 6825 2050 6825
Wire Wire Line
	2050 6850 2050 6825
Wire Wire Line
	1800 7075 2050 7075
Wire Wire Line
	2050 7050 2050 7075
Wire Wire Line
	2050 7075 2075 7075
Wire Wire Line
	1475 7100 1475 7075
Wire Wire Line
	1500 7075 1475 7075
Wire Wire Line
	1300 7100 1300 6975
Wire Wire Line
	1300 6825 1500 6825
Wire Wire Line
	1075 7175 1200 7175
Wire Wire Line
	1200 7175 1200 7475
Wire Wire Line
	1200 7475 1300 7475
Wire Wire Line
	1300 7400 1300 7475
Connection ~ 1300 7475
Wire Wire Line
	1300 7475 1475 7475
Wire Wire Line
	1475 7475 1475 7400
Wire Wire Line
	1075 7075 1475 7075
Connection ~ 1475 7075
Wire Wire Line
	1075 6975 1300 6975
Connection ~ 1300 6975
Wire Wire Line
	1300 6975 1300 6825
Wire Wire Line
	1075 6875 1200 6875
Wire Wire Line
	1200 6875 1200 6675
Text GLabel 3875 6825 2    50   Input ~ 0
IN_6P
Wire Wire Line
	3800 6825 3850 6825
Wire Wire Line
	3850 6850 3850 6825
Connection ~ 3850 6825
Text GLabel 3375 6950 2    50   Input ~ 0
1.25V
Connection ~ 2050 6825
Connection ~ 2050 7075
Connection ~ 1475 7475
$Comp
L power:GND #PWR0104
U 1 1 637034AA
P 1475 7675
F 0 "#PWR0104" H 1475 7425 50  0001 C CNN
F 1 "GND" H 1475 7525 50  0000 C CNN
F 2 "" H 1475 7675 50  0000 C CNN
F 3 "" H 1475 7675 50  0000 C CNN
	1    1475 7675
	1    0    0    -1  
$EndComp
Text GLabel 1300 6675 2    50   Input ~ 0
5V
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C48
U 1 1 637034B5
P 3350 7350
F 0 "C48" H 3360 7420 50  0000 L CNN
F 1 "100nF 25v" H 3360 7270 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3350 7350 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3350 7350 50  0001 C CNN
	1    3350 7350
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R22
U 1 1 637034BF
P 3650 7075
F 0 "R22" V 3730 7075 50  0000 C CNN
F 1 "100h" V 3650 7075 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 3580 7075 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_100R-100R-1_C125923.html" H 3650 7075 50  0001 C CNN
	1    3650 7075
	0    -1   1    0   
$EndComp
Text GLabel 3875 7075 2    50   Input ~ 0
IN_6N
Wire Wire Line
	3800 7075 3850 7075
Connection ~ 3850 7075
Wire Wire Line
	3850 7075 3875 7075
Wire Wire Line
	3850 7050 3850 7075
Wire Wire Line
	3850 6825 3875 6825
$Comp
L Components:PGA281 U3
U 1 1 637034CF
P 2250 6875
F 0 "U3" H 2225 7200 50  0000 C CNN
F 1 "PGA281" H 2412 7176 50  0001 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 2125 6200 50  0001 L CIN
F 3 "" H 2395 6880 50  0001 L CNN
	1    2250 6875
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 6825 2075 6825
Wire Wire Line
	1475 7475 1900 7475
Wire Wire Line
	1200 6675 1300 6675
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C2
U 1 1 637034DC
P 1900 7350
F 0 "C2" H 1975 7350 50  0000 L CNN
F 1 "100nF 25v" H 1910 7270 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 1900 7350 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 1900 7350 50  0001 C CNN
	1    1900 7350
	-1   0    0    1   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C16
U 1 1 637034E6
P 2050 7350
F 0 "C16" H 1975 7525 50  0000 L CNN
F 1 "100nF 25v" H 2060 7270 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 2050 7350 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 2050 7350 50  0001 C CNN
	1    2050 7350
	-1   0    0    1   
$EndComp
Wire Wire Line
	1900 7250 1900 6675
Wire Wire Line
	1900 6675 2075 6675
Wire Wire Line
	2075 7225 2050 7225
Wire Wire Line
	2050 7225 2050 7250
Wire Wire Line
	2050 7450 2050 7475
Wire Wire Line
	2050 7475 1900 7475
Wire Wire Line
	1900 7450 1900 7475
Connection ~ 1900 7475
Text GLabel 1850 6675 0    50   Input ~ 0
12V
Wire Wire Line
	1850 6675 1900 6675
Connection ~ 1900 6675
Text GLabel 1850 7225 0    50   Input ~ 0
-12V
Wire Wire Line
	1850 7225 2050 7225
Connection ~ 2050 7225
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C24
U 1 1 637034FE
P 3200 7350
F 0 "C24" H 3210 7420 50  0000 L CNN
F 1 "100nF 25v" H 3210 7270 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3200 7350 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3200 7350 50  0001 C CNN
	1    3200 7350
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 63703508
P 3200 7725
F 0 "#PWR0114" H 3200 7475 50  0001 C CNN
F 1 "GND" H 3200 7575 50  0000 C CNN
F 2 "" H 3200 7725 50  0000 C CNN
F 3 "" H 3200 7725 50  0000 C CNN
	1    3200 7725
	1    0    0    -1  
$EndComp
Wire Wire Line
	3075 7225 3100 7225
Wire Wire Line
	3100 7225 3100 7475
Wire Wire Line
	3075 6675 3200 6675
Wire Wire Line
	3200 6675 3200 7250
Wire Wire Line
	3075 6950 3350 6950
Wire Wire Line
	3350 6950 3350 7250
Wire Wire Line
	3075 6825 3500 6825
Wire Wire Line
	3500 7075 3075 7075
Wire Wire Line
	3100 7475 3200 7475
Wire Wire Line
	3200 7475 3200 7450
Wire Wire Line
	3200 7475 3350 7475
Wire Wire Line
	3350 7475 3350 7450
Connection ~ 3200 7475
Wire Wire Line
	3375 6950 3350 6950
Connection ~ 3350 6950
Text GLabel 3375 6675 2    50   Input ~ 0
3.3Va
Wire Wire Line
	3375 6675 3200 6675
Connection ~ 3200 6675
Text GLabel 2875 7675 3    50   Input ~ 0
3.3V
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C21
U 1 1 63703525
P 3050 7650
F 0 "C21" H 3060 7720 50  0000 L CNN
F 1 "100nF 25v" H 3060 7570 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3050 7650 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3050 7650 50  0001 C CNN
	1    3050 7650
	0    1    -1   0   
$EndComp
Wire Wire Line
	3200 7475 3200 7650
Wire Wire Line
	3150 7650 3200 7650
Connection ~ 3200 7650
Wire Wire Line
	3200 7650 3200 7725
Wire Wire Line
	2875 7675 2875 7650
Wire Wire Line
	2950 7650 2875 7650
Connection ~ 2875 7650
Wire Wire Line
	2875 7650 2875 7625
Text GLabel 2775 7675 3    50   Input ~ 0
IN6_G0
Text GLabel 2675 7675 3    50   Input ~ 0
IN6_G1
Text GLabel 2575 7675 3    50   Input ~ 0
IN6_G2
Text GLabel 2475 7675 3    50   Input ~ 0
IN6_G3
Text GLabel 2375 7675 3    50   Input ~ 0
IN6_G4
Wire Wire Line
	2375 7675 2375 7625
Wire Wire Line
	2475 7675 2475 7625
Wire Wire Line
	2575 7675 2575 7625
Wire Wire Line
	2675 7675 2675 7625
Wire Wire Line
	2775 7675 2775 7625
Wire Wire Line
	2275 7675 2275 7650
$Comp
L Device:LED_Small_ALT D4
U 1 1 63703542
P 1650 7650
F 0 "D4" H 1550 7700 50  0000 C CNN
F 1 "LED_Small_ALT" H 1650 7534 50  0001 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" V 1650 7650 50  0001 C CNN
F 3 "~" V 1650 7650 50  0001 C CNN
	1    1650 7650
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R15
U 1 1 6370354C
P 1950 7650
F 0 "R15" V 1900 7500 50  0000 C CNN
F 1 "2.2k" V 1950 7650 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1880 7650 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1950 7650 50  0001 C CNN
	1    1950 7650
	0    1    -1   0   
$EndComp
Wire Wire Line
	2100 7650 2275 7650
Connection ~ 2275 7650
Wire Wire Line
	2275 7650 2275 7625
Wire Wire Line
	1800 7650 1750 7650
Wire Wire Line
	1475 7675 1475 7650
Wire Wire Line
	1550 7650 1475 7650
Connection ~ 1475 7650
Wire Wire Line
	1475 7650 1475 7475
Text GLabel 2275 7675 3    50   Input ~ 0
IN6_ERR
$Comp
L 3Phase-rescue:R-3Phase-rescue R37
U 1 1 6370355F
P 3650 8375
F 0 "R37" V 3575 8375 50  0000 C CNN
F 1 "100h" V 3650 8375 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 3580 8375 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_100R-100R-1_C125923.html" H 3650 8375 50  0001 C CNN
	1    3650 8375
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C52
U 1 1 63703569
P 3850 8500
F 0 "C52" H 3625 8500 50  0000 L CNN
F 1 "100nF 25v" H 3860 8420 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3850 8500 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3850 8500 50  0001 C CNN
	1    3850 8500
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:CONN_01X04-3Phase-rescue J4
U 1 1 63703573
P 875 8575
F 0 "J4" H 875 8825 50  0000 C CNN
F 1 "95001-6P6C" H 675 8925 50  0001 C CNN
F 2 "Footprints:KF2EDGR_2.54x4" H 875 8575 50  0001 C CNN
F 3 "" H 875 8575 50  0001 C CNN
	1    875  8575
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R3
U 1 1 6370357D
P 1300 8800
F 0 "R3" V 1380 8800 50  0000 C CNN
F 1 "2.2k" V 1300 8800 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1230 8800 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1300 8800 50  0001 C CNN
	1    1300 8800
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R6
U 1 1 63703587
P 1475 8800
F 0 "R6" V 1555 8800 50  0000 C CNN
F 1 "2.2k" V 1475 8800 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1405 8800 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1475 8800 50  0001 C CNN
	1    1475 8800
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R11
U 1 1 63703591
P 1650 8375
F 0 "R11" V 1730 8375 50  0000 C CNN
F 1 "2.2k" V 1650 8375 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1580 8375 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1650 8375 50  0001 C CNN
	1    1650 8375
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R12
U 1 1 6370359B
P 1650 8625
F 0 "R12" V 1575 8625 50  0000 C CNN
F 1 "2.2k" V 1650 8625 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1580 8625 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1650 8625 50  0001 C CNN
	1    1650 8625
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C18
U 1 1 637035A5
P 2050 8500
F 0 "C18" H 2125 8500 50  0000 L CNN
F 1 "100nF 25v" H 2060 8420 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 2050 8500 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 2050 8500 50  0001 C CNN
	1    2050 8500
	-1   0    0    1   
$EndComp
Wire Wire Line
	1800 8375 2050 8375
Wire Wire Line
	2050 8400 2050 8375
Wire Wire Line
	1800 8625 2050 8625
Wire Wire Line
	2050 8600 2050 8625
Wire Wire Line
	2050 8625 2075 8625
Wire Wire Line
	1475 8650 1475 8625
Wire Wire Line
	1500 8625 1475 8625
Wire Wire Line
	1300 8650 1300 8525
Wire Wire Line
	1300 8375 1500 8375
Wire Wire Line
	1075 8725 1200 8725
Wire Wire Line
	1200 8725 1200 9025
Wire Wire Line
	1200 9025 1300 9025
Wire Wire Line
	1300 8950 1300 9025
Connection ~ 1300 9025
Wire Wire Line
	1300 9025 1475 9025
Wire Wire Line
	1475 9025 1475 8950
Wire Wire Line
	1075 8625 1475 8625
Connection ~ 1475 8625
Wire Wire Line
	1075 8525 1300 8525
Connection ~ 1300 8525
Wire Wire Line
	1300 8525 1300 8375
Wire Wire Line
	1075 8425 1200 8425
Wire Wire Line
	1200 8425 1200 8225
Text GLabel 3875 8375 2    50   Input ~ 0
IN_7P
Wire Wire Line
	3800 8375 3850 8375
Wire Wire Line
	3850 8400 3850 8375
Connection ~ 3850 8375
Text GLabel 3375 8500 2    50   Input ~ 0
1.25V
Connection ~ 2050 8375
Connection ~ 2050 8625
Connection ~ 1475 9025
$Comp
L power:GND #PWR0115
U 1 1 637035CE
P 1475 9225
F 0 "#PWR0115" H 1475 8975 50  0001 C CNN
F 1 "GND" H 1475 9075 50  0000 C CNN
F 2 "" H 1475 9225 50  0000 C CNN
F 3 "" H 1475 9225 50  0000 C CNN
	1    1475 9225
	1    0    0    -1  
$EndComp
Text GLabel 1300 8225 2    50   Input ~ 0
5V
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C49
U 1 1 637035D9
P 3350 8900
F 0 "C49" H 3360 8970 50  0000 L CNN
F 1 "100nF 25v" H 3360 8820 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3350 8900 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3350 8900 50  0001 C CNN
	1    3350 8900
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R41
U 1 1 637035E3
P 3650 8625
F 0 "R41" V 3730 8625 50  0000 C CNN
F 1 "100h" V 3650 8625 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 3580 8625 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_100R-100R-1_C125923.html" H 3650 8625 50  0001 C CNN
	1    3650 8625
	0    -1   1    0   
$EndComp
Text GLabel 3875 8625 2    50   Input ~ 0
IN_7N
Wire Wire Line
	3800 8625 3850 8625
Connection ~ 3850 8625
Wire Wire Line
	3850 8625 3875 8625
Wire Wire Line
	3850 8600 3850 8625
Wire Wire Line
	3850 8375 3875 8375
$Comp
L Components:PGA281 U4
U 1 1 637035F3
P 2250 8425
F 0 "U4" H 2225 8750 50  0000 C CNN
F 1 "PGA281" H 2412 8726 50  0001 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 2125 7750 50  0001 L CIN
F 3 "" H 2395 8430 50  0001 L CNN
	1    2250 8425
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 8375 2075 8375
Wire Wire Line
	1475 9025 1900 9025
Wire Wire Line
	1200 8225 1300 8225
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C4
U 1 1 63703600
P 1900 8900
F 0 "C4" H 1975 8900 50  0000 L CNN
F 1 "100nF 25v" H 1910 8820 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 1900 8900 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 1900 8900 50  0001 C CNN
	1    1900 8900
	-1   0    0    1   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C19
U 1 1 6370360A
P 2050 8900
F 0 "C19" H 1975 9075 50  0000 L CNN
F 1 "100nF 25v" H 2060 8820 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 2050 8900 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 2050 8900 50  0001 C CNN
	1    2050 8900
	-1   0    0    1   
$EndComp
Wire Wire Line
	1900 8800 1900 8225
Wire Wire Line
	1900 8225 2075 8225
Wire Wire Line
	2075 8775 2050 8775
Wire Wire Line
	2050 8775 2050 8800
Wire Wire Line
	2050 9000 2050 9025
Wire Wire Line
	2050 9025 1900 9025
Wire Wire Line
	1900 9000 1900 9025
Connection ~ 1900 9025
Text GLabel 1850 8225 0    50   Input ~ 0
12V
Wire Wire Line
	1850 8225 1900 8225
Connection ~ 1900 8225
Text GLabel 1850 8775 0    50   Input ~ 0
-12V
Wire Wire Line
	1850 8775 2050 8775
Connection ~ 2050 8775
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C25
U 1 1 63703622
P 3200 8900
F 0 "C25" H 3210 8970 50  0000 L CNN
F 1 "100nF 25v" H 3210 8820 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3200 8900 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3200 8900 50  0001 C CNN
	1    3200 8900
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 6370362C
P 3200 9275
F 0 "#PWR0116" H 3200 9025 50  0001 C CNN
F 1 "GND" H 3200 9125 50  0000 C CNN
F 2 "" H 3200 9275 50  0000 C CNN
F 3 "" H 3200 9275 50  0000 C CNN
	1    3200 9275
	1    0    0    -1  
$EndComp
Wire Wire Line
	3075 8775 3100 8775
Wire Wire Line
	3100 8775 3100 9025
Wire Wire Line
	3075 8225 3200 8225
Wire Wire Line
	3200 8225 3200 8800
Wire Wire Line
	3075 8500 3350 8500
Wire Wire Line
	3350 8500 3350 8800
Wire Wire Line
	3075 8375 3500 8375
Wire Wire Line
	3500 8625 3075 8625
Wire Wire Line
	3100 9025 3200 9025
Wire Wire Line
	3200 9025 3200 9000
Wire Wire Line
	3200 9025 3350 9025
Wire Wire Line
	3350 9025 3350 9000
Connection ~ 3200 9025
Wire Wire Line
	3375 8500 3350 8500
Connection ~ 3350 8500
Text GLabel 3375 8225 2    50   Input ~ 0
3.3Va
Wire Wire Line
	3375 8225 3200 8225
Connection ~ 3200 8225
Text GLabel 2875 9225 3    50   Input ~ 0
3.3V
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C22
U 1 1 63703649
P 3050 9200
F 0 "C22" H 3060 9270 50  0000 L CNN
F 1 "100nF 25v" H 3060 9120 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 3050 9200 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 3050 9200 50  0001 C CNN
	1    3050 9200
	0    1    -1   0   
$EndComp
Wire Wire Line
	3200 9025 3200 9200
Wire Wire Line
	3150 9200 3200 9200
Connection ~ 3200 9200
Wire Wire Line
	3200 9200 3200 9275
Wire Wire Line
	2875 9225 2875 9200
Wire Wire Line
	2950 9200 2875 9200
Connection ~ 2875 9200
Wire Wire Line
	2875 9200 2875 9175
Text GLabel 2775 9225 3    50   Input ~ 0
IN7_G0
Text GLabel 2675 9225 3    50   Input ~ 0
IN7_G1
Text GLabel 2575 9225 3    50   Input ~ 0
IN7_G2
Text GLabel 2475 9225 3    50   Input ~ 0
IN7_G3
Text GLabel 2375 9225 3    50   Input ~ 0
IN7_G4
Wire Wire Line
	2375 9225 2375 9175
Wire Wire Line
	2475 9225 2475 9175
Wire Wire Line
	2575 9225 2575 9175
Wire Wire Line
	2675 9225 2675 9175
Wire Wire Line
	2775 9225 2775 9175
Wire Wire Line
	2275 9225 2275 9200
$Comp
L Device:LED_Small_ALT D5
U 1 1 63703666
P 1650 9200
F 0 "D5" H 1550 9250 50  0000 C CNN
F 1 "LED_Small_ALT" H 1650 9084 50  0001 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" V 1650 9200 50  0001 C CNN
F 3 "~" V 1650 9200 50  0001 C CNN
	1    1650 9200
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R16
U 1 1 63703670
P 1950 9200
F 0 "R16" V 1900 9050 50  0000 C CNN
F 1 "2.2k" V 1950 9200 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 1880 9200 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 1950 9200 50  0001 C CNN
	1    1950 9200
	0    1    -1   0   
$EndComp
Wire Wire Line
	2100 9200 2275 9200
Connection ~ 2275 9200
Wire Wire Line
	2275 9200 2275 9175
Wire Wire Line
	1800 9200 1750 9200
Wire Wire Line
	1475 9225 1475 9200
Wire Wire Line
	1550 9200 1475 9200
Connection ~ 1475 9200
Wire Wire Line
	1475 9200 1475 9025
Text GLabel 2275 9225 3    50   Input ~ 0
IN7_ERR
$Comp
L Device:D_Schottky_Small_ALT D8
U 1 1 638EAC7A
P 7800 1600
F 0 "D8" H 7800 1500 50  0000 C CNN
F 1 "D_Schottky_Small_ALT" H 7800 1484 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-323" V 7800 1600 50  0001 C CNN
F 3 "~" V 7800 1600 50  0001 C CNN
	1    7800 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:L_Small L1
U 1 1 638ECEC3
P 7075 825
F 0 "L1" V 7168 825 50  0000 C CNN
F 1 "L_Small" V 7169 825 50  0001 C CNN
F 2 "Footprints:L_1210_3225Metric" H 7075 825 50  0001 C CNN
F 3 "~" H 7075 825 50  0001 C CNN
	1    7075 825 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6725 825  6975 825 
Wire Wire Line
	7175 825  7400 825 
Wire Wire Line
	7400 825  7400 975 
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C54
U 1 1 63A804C3
P 7400 1475
F 0 "C54" H 7425 1400 50  0000 L CNN
F 1 "100nF 25v" H 7410 1395 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 7400 1475 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 7400 1475 50  0001 C CNN
	1    7400 1475
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R42
U 1 1 63A8C30A
P 7650 1100
F 0 "R42" V 7725 1000 50  0000 C CNN
F 1 "2.2k" V 7650 1100 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 7580 1100 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 7650 1100 50  0001 C CNN
	1    7650 1100
	0    1    -1   0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C55
U 1 1 63A918A8
P 7975 1225
F 0 "C55" H 7800 1225 50  0000 L CNN
F 1 "100nF 25v" H 7985 1145 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 7975 1225 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 7975 1225 50  0001 C CNN
	1    7975 1225
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C56
U 1 1 6405CFCC
P 7975 1475
F 0 "C56" H 7750 1475 50  0000 L CNN
F 1 "100nF 25v" H 7985 1395 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 7975 1475 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 7975 1475 50  0001 C CNN
	1    7975 1475
	-1   0    0    -1  
$EndComp
$Comp
L Components:LM27313XMF U12
U 1 1 639B43C1
P 7050 1100
F 0 "U12" H 7325 925 50  0000 C CNN
F 1 "LM27313XMF" H 7050 1376 50  0001 C CNN
F 2 "Footprints:SOT-23-5" H 7100 850 50  0001 L CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm27313.pdf" H 7050 1200 50  0001 C CNN
	1    7050 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Schottky_Small_ALT D6
U 1 1 6412E023
P 7675 825
F 0 "D6" H 7675 725 50  0000 C CNN
F 1 "D_Schottky_Small_ALT" H 7675 709 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-323" V 7675 825 50  0001 C CNN
F 3 "~" V 7675 825 50  0001 C CNN
	1    7675 825 
	-1   0    0    1   
$EndComp
Connection ~ 7400 975 
Wire Wire Line
	7400 975  7350 975 
Wire Wire Line
	7825 825  7825 950 
Text GLabel 8100 825  2    50   Input ~ 0
12V
Wire Wire Line
	8100 825  7975 825 
Wire Wire Line
	7975 1350 7975 1325
Connection ~ 7975 825 
Wire Wire Line
	7975 825  7825 825 
$Comp
L Device:D_Schottky_Small_ALT D7
U 1 1 638E9F4C
P 7650 1475
F 0 "D7" V 7650 1545 50  0000 L CNN
F 1 "D_Schottky_Small_ALT" H 7650 1591 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-323" V 7650 1475 50  0001 C CNN
F 3 "~" V 7650 1475 50  0001 C CNN
	1    7650 1475
	0    1    1    0   
$EndComp
Wire Wire Line
	7975 825  7975 1125
Text GLabel 8125 1600 2    50   Input ~ 0
-12V
Wire Wire Line
	7825 1100 7800 1100
Wire Wire Line
	7975 1375 7975 1350
Connection ~ 7975 1350
Wire Wire Line
	8125 1600 7975 1600
Wire Wire Line
	7975 1600 7975 1575
Wire Wire Line
	7900 1600 7975 1600
Connection ~ 7975 1600
Wire Wire Line
	7700 1600 7650 1600
Wire Wire Line
	7650 1600 7650 1575
Wire Wire Line
	7650 1375 7650 1350
Connection ~ 7650 1350
Wire Wire Line
	7400 1575 7400 1600
Wire Wire Line
	7400 1600 7650 1600
Connection ~ 7650 1600
Wire Wire Line
	7400 975  7400 1375
Wire Wire Line
	6750 1000 6725 1000
Wire Wire Line
	6725 1000 6725 825 
Wire Wire Line
	6750 1200 6725 1200
Wire Wire Line
	6725 1200 6725 1350
Wire Wire Line
	6725 1350 7650 1350
Wire Wire Line
	7350 1100 7475 1100
Wire Wire Line
	7500 1225 7475 1225
Wire Wire Line
	7650 1350 7825 1350
Wire Wire Line
	7825 1350 7975 1350
Connection ~ 7825 1350
Wire Wire Line
	7825 1225 7825 1350
Wire Wire Line
	7800 1225 7825 1225
$Comp
L 3Phase-rescue:R-3Phase-rescue R43
U 1 1 63A8CDC9
P 7650 1225
F 0 "R43" V 7675 1400 50  0000 C CNN
F 1 "2.2k" V 7650 1225 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 7580 1225 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 7650 1225 50  0001 C CNN
	1    7650 1225
	0    1    -1   0   
$EndComp
Wire Wire Line
	7475 1225 7475 1100
Connection ~ 7475 1100
Wire Wire Line
	7475 1100 7500 1100
Wire Wire Line
	6750 1100 6725 1100
Wire Wire Line
	6725 1100 6725 1000
Connection ~ 6725 1000
Text GLabel 6575 825  0    50   Input ~ 0
5V
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C53
U 1 1 66B94CD4
P 6600 1125
F 0 "C53" H 6625 1200 50  0000 L CNN
F 1 "100nF 25v" H 6610 1045 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 6600 1125 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 6600 1125 50  0001 C CNN
	1    6600 1125
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6600 1025 6600 825 
Wire Wire Line
	6600 825  6725 825 
Connection ~ 6725 825 
Wire Wire Line
	6600 1225 6600 1350
Wire Wire Line
	6600 1350 6725 1350
Connection ~ 6725 1350
Wire Wire Line
	6575 825  6600 825 
Connection ~ 6600 825 
$Comp
L power:GND #PWR0117
U 1 1 66CCE20C
P 8125 1350
F 0 "#PWR0117" H 8125 1100 50  0001 C CNN
F 1 "GND" V 8130 1222 50  0000 R CNN
F 2 "" H 8125 1350 50  0001 C CNN
F 3 "" H 8125 1350 50  0001 C CNN
	1    8125 1350
	0    -1   1    0   
$EndComp
Wire Wire Line
	8125 1350 7975 1350
Wire Wire Line
	7825 825  7775 825 
Connection ~ 7825 825 
Wire Wire Line
	7575 825  7400 825 
Connection ~ 7400 825 
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C57
U 1 1 66F88300
P 7650 950
F 0 "C57" V 7700 800 50  0000 L CNN
F 1 "100nF 25v" H 7660 870 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 7650 950 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 7650 950 50  0001 C CNN
	1    7650 950 
	0    1    -1   0   
$EndComp
Wire Wire Line
	7750 950  7825 950 
Connection ~ 7825 950 
Wire Wire Line
	7825 950  7825 1100
Wire Wire Line
	7550 950  7475 950 
Wire Wire Line
	7475 950  7475 1100
Text GLabel 5075 9625 0    50   Input ~ 0
IN4_G0
Text GLabel 5075 9525 0    50   Input ~ 0
IN4_G1
Text GLabel 5075 9425 0    50   Input ~ 0
IN4_G2
Text GLabel 5075 9325 0    50   Input ~ 0
IN4_G3
Text GLabel 5075 9225 0    50   Input ~ 0
IN4_G4
Text GLabel 5075 9025 0    50   Input ~ 0
IN5_ERR
Text GLabel 2675 6125 3    50   Input ~ 0
IN5_G1
$Comp
L 3Phase-rescue:CONN_01X04-3Phase-rescue J7
U 1 1 67B09A53
P 4850 6175
F 0 "J7" H 4850 6425 50  0000 C CNN
F 1 "95001-6P6C" H 4650 6525 50  0001 C CNN
F 2 "Footprints:KF2EDGR_2.54x4" H 4850 6175 50  0001 C CNN
F 3 "" H 4850 6175 50  0001 C CNN
F 4 "GPS" V 4950 6175 50  0000 C CNN "Name"
	1    4850 6175
	-1   0    0    -1  
$EndComp
$Comp
L Components:Conn_01x03 J5
U 1 1 67B0D029
P 4850 5550
F 0 "J5" H 4850 5800 50  0000 C CNN
F 1 "95001-6P6C" H 4650 5900 50  0001 C CNN
F 2 "Footprints:PinHeader_1x03_P2.54mm_Vertical" H 4850 5550 50  0001 C CNN
F 3 "" H 4850 5550 50  0001 C CNN
F 4 "Debug" V 4950 5550 50  0000 C CNN "Name"
	1    4850 5550
	-1   0    0    -1  
$EndComp
Text GLabel 4925 5775 0    50   Input ~ 0
5V
$Comp
L power:GND #PWR0118
U 1 1 67EFDF64
P 5075 6350
F 0 "#PWR0118" H 5075 6100 50  0001 C CNN
F 1 "GND" H 4950 6275 50  0000 C CNN
F 2 "" H 5075 6350 50  0000 C CNN
F 3 "" H 5075 6350 50  0000 C CNN
	1    5075 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 6125 5100 6125
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C58
U 1 1 687B03EC
P 5700 5775
F 0 "C58" V 5650 5600 50  0000 L CNN
F 1 "100nF 25v" H 5710 5695 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 5700 5775 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 5700 5775 50  0001 C CNN
	1    5700 5775
	0    1    -1   0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R44
U 1 1 6890523E
P 6125 5950
F 0 "R44" V 6125 5950 50  0000 C CNN
F 1 "2.2k" V 6125 5950 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 6055 5950 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 6125 5950 50  0001 C CNN
	1    6125 5950
	-1   0    0    -1  
$EndComp
Text GLabel 5075 6525 0    50   Input ~ 0
EN
Text GLabel 5075 6625 0    50   Input ~ 0
LCD_BL
Text GLabel 5075 6825 0    50   Input ~ 0
Beep
$Comp
L 3Phase-rescue:R-3Phase-rescue R45
U 1 1 693F4865
P 5825 6725
F 0 "R45" V 5775 6550 50  0000 C CNN
F 1 "2.2k" V 5825 6725 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5755 6725 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 5825 6725 50  0001 C CNN
	1    5825 6725
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 693F5602
P 5625 6725
F 0 "#PWR0119" H 5625 6475 50  0001 C CNN
F 1 "GND" V 5625 6525 50  0000 C CNN
F 2 "" H 5625 6725 50  0000 C CNN
F 3 "" H 5625 6725 50  0000 C CNN
	1    5625 6725
	0    1    1    0   
$EndComp
Wire Wire Line
	5675 6725 5625 6725
Text GLabel 5075 7025 0    50   Input ~ 0
MOSI
Text GLabel 4700 2325 2    50   Input ~ 0
MCLK
Text GLabel 5075 7125 0    50   Input ~ 0
MCLK
Text GLabel 5075 6925 0    50   Input ~ 0
LCD_A0
Text GLabel 5075 7225 0    50   Input ~ 0
LCD_CS
Text GLabel 5075 7325 0    50   Input ~ 0
IN6_ERR
Text GLabel 5075 7825 0    50   Input ~ 0
IN6_G0
Text GLabel 5075 7725 0    50   Input ~ 0
IN6_G1
Text GLabel 5075 7625 0    50   Input ~ 0
IN6_G2
Text GLabel 5075 7525 0    50   Input ~ 0
IN6_G3
Text GLabel 5075 7425 0    50   Input ~ 0
IN6_G4
Text GLabel 5075 7925 0    50   Input ~ 0
IN7_ERR
Text GLabel 5075 8425 0    50   Input ~ 0
IN7_G0
Text GLabel 5075 8325 0    50   Input ~ 0
IN7_G1
Text GLabel 5075 8225 0    50   Input ~ 0
IN7_G2
Text GLabel 5075 8125 0    50   Input ~ 0
IN7_G3
$Comp
L Device:LED_Small_ALT D10
U 1 1 6A9475CF
P 5725 9775
F 0 "D10" H 5625 9825 50  0000 C CNN
F 1 "LED_Small_ALT" H 5725 9659 50  0001 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" V 5725 9775 50  0001 C CNN
F 3 "~" V 5725 9775 50  0001 C CNN
	1    5725 9775
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R46
U 1 1 6A9CE9AB
P 6000 9775
F 0 "R46" V 6075 9775 50  0000 C CNN
F 1 "2.2k" V 6000 9775 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5930 9775 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 6000 9775 50  0001 C CNN
	1    6000 9775
	0    1    -1   0   
$EndComp
Wire Wire Line
	6150 9900 6175 9900
Wire Wire Line
	6175 9900 6175 9775
Connection ~ 6175 9775
Wire Wire Line
	5825 9775 5850 9775
$Comp
L power:GND #PWR0120
U 1 1 6AB42AC8
P 5575 9775
F 0 "#PWR0120" H 5575 9525 50  0001 C CNN
F 1 "GND" V 5580 9647 50  0000 R CNN
F 2 "" H 5575 9775 50  0001 C CNN
F 3 "" H 5575 9775 50  0001 C CNN
	1    5575 9775
	0    1    -1   0   
$EndComp
Wire Wire Line
	5575 9775 5625 9775
Wire Wire Line
	5000 9950 5025 9950
Wire Wire Line
	5025 9950 5025 9900
Wire Wire Line
	5025 9900 5075 9900
Wire Wire Line
	5000 10050 5025 10050
Wire Wire Line
	5025 10050 5025 10100
Wire Wire Line
	5025 10100 5075 10100
Text GLabel 8950 9475 2    50   Input ~ 0
IN_7N
Text GLabel 8950 9575 2    50   Input ~ 0
IN_7P
Text GLabel 8950 9675 2    50   Input ~ 0
IN_6N
Text GLabel 8950 9775 2    50   Input ~ 0
IN_6P
Text GLabel 8950 10175 2    50   Input ~ 0
IN_5N
Text GLabel 8950 10075 2    50   Input ~ 0
IN_5P
Text GLabel 8950 9975 2    50   Input ~ 0
IN_4N
Text GLabel 8950 9875 2    50   Input ~ 0
IN_4P
Text GLabel 3475 1000 2    50   Input ~ 0
IN_1N
Text GLabel 8950 9375 2    50   Input ~ 0
IN_1N
Text GLabel 8950 9275 2    50   Input ~ 0
IN_1P
Text GLabel 8950 9075 2    50   Input ~ 0
IN_2P
Text GLabel 8950 9175 2    50   Input ~ 0
IN_2N
Text GLabel 8950 8875 2    50   Input ~ 0
IN_3P
Text GLabel 8950 8975 2    50   Input ~ 0
IN_3N
Wire Wire Line
	5075 5325 5075 5350
Connection ~ 5075 5350
Wire Wire Line
	5075 5350 5200 5350
Wire Wire Line
	5200 5075 5200 5000
Connection ~ 5200 5000
Wire Wire Line
	5200 5000 5575 5000
Wire Wire Line
	5200 5275 5200 5350
Connection ~ 5200 5350
$Comp
L 3Phase-rescue:R-3Phase-rescue R48
U 1 1 6B7E62EB
P 6025 5250
F 0 "R48" V 6025 5250 50  0000 C CNN
F 1 "2.2k" V 6025 5250 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5955 5250 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 6025 5250 50  0001 C CNN
	1    6025 5250
	0    1    1    0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R47
U 1 1 6B7E6B2E
P 5675 5250
F 0 "R47" V 5675 5250 50  0000 C CNN
F 1 "2.2k" V 5675 5250 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5605 5250 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 5675 5250 50  0001 C CNN
	1    5675 5250
	0    1    1    0   
$EndComp
Wire Wire Line
	5575 5000 6025 5000
Wire Wire Line
	5825 5250 5850 5250
Wire Wire Line
	5850 5150 5850 5250
Connection ~ 5850 5250
Wire Wire Line
	5850 5250 5875 5250
Text GLabel 5500 5250 0    50   Input ~ 0
2.5V
$Comp
L 3Phase-rescue:R-3Phase-rescue R39
U 1 1 622D07DA
P 5075 5175
F 0 "R39" V 5075 5175 50  0000 C CNN
F 1 "2.2k" V 5075 5175 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5005 5175 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 5075 5175 50  0001 C CNN
	1    5075 5175
	1    0    0    -1  
$EndComp
Wire Wire Line
	5075 5025 5075 5000
Connection ~ 5075 5000
Wire Wire Line
	5500 5250 5525 5250
$Comp
L power:GND #PWR0121
U 1 1 6C57D1CF
P 8875 8775
F 0 "#PWR0121" H 8875 8525 50  0001 C CNN
F 1 "GND" V 8880 8647 50  0000 R CNN
F 2 "" H 8875 8775 50  0001 C CNN
F 3 "" H 8875 8775 50  0001 C CNN
	1    8875 8775
	0    -1   1    0   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C59
U 1 1 6C73D44D
P 8350 8650
F 0 "C59" V 8475 8575 50  0000 L CNN
F 1 "100nF 25v" H 8360 8570 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 8350 8650 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 8350 8650 50  0001 C CNN
	1    8350 8650
	1    0    0    1   
$EndComp
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C37
U 1 1 6C73FA61
P 8575 8650
F 0 "C37" V 8675 8575 50  0000 L CNN
F 1 "100nF 25v" H 8585 8570 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 8575 8650 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 8575 8650 50  0001 C CNN
	1    8575 8650
	1    0    0    1   
$EndComp
Text GLabel 8950 8175 2    50   Input ~ 0
3.3V
Wire Wire Line
	8825 7550 8825 7825
Wire Wire Line
	6075 5000 6025 5000
Connection ~ 6025 5000
$Comp
L 3Phase-rescue:R-3Phase-rescue R50
U 1 1 6D127E58
P 7925 7225
F 0 "R50" V 7925 7225 50  0000 C CNN
F 1 "2.2k" V 7925 7225 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 7855 7225 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 7925 7225 50  0001 C CNN
	1    7925 7225
	-1   0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R58
U 1 1 6D1288F5
P 8050 7225
F 0 "R58" V 8050 7225 50  0000 C CNN
F 1 "2.2k" V 8050 7225 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 7980 7225 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 8050 7225 50  0001 C CNN
	1    8050 7225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9175 7400 8150 7400
Connection ~ 8150 7400
Wire Wire Line
	8175 6925 8050 6925
Wire Wire Line
	8050 7400 8050 7375
Connection ~ 8050 7400
Wire Wire Line
	8050 7400 8150 7400
Wire Wire Line
	7925 7400 7925 7375
Wire Wire Line
	8050 7075 8050 6925
Connection ~ 8050 6925
Wire Wire Line
	8150 7125 8175 7125
Wire Wire Line
	8150 7125 8150 7400
Wire Wire Line
	8950 6825 8975 6825
Wire Wire Line
	8950 6725 8975 6725
Wire Wire Line
	8975 6725 8975 6825
Wire Wire Line
	8975 7125 8950 7125
$Comp
L power:GND #PWR0123
U 1 1 6E24B954
P 8975 7250
F 0 "#PWR0123" H 8975 7000 50  0001 C CNN
F 1 "GND" H 8900 7200 50  0000 R CNN
F 2 "" H 8975 7250 50  0001 C CNN
F 3 "" H 8975 7250 50  0001 C CNN
	1    8975 7250
	1    0    0    -1  
$EndComp
Text GLabel 9075 6725 2    50   Input ~ 0
5V
Wire Wire Line
	9075 6725 8975 6725
Connection ~ 8975 6725
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C60
U 1 1 6C47C62E
P 7900 8650
F 0 "C60" V 8000 8575 50  0000 L CNN
F 1 "100nF 25v" H 7910 8570 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 7900 8650 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 7900 8650 50  0001 C CNN
	1    7900 8650
	1    0    0    1   
$EndComp
$Comp
L Components:TMP100 U5
U 1 1 6F557D59
P 8575 6325
F 0 "U5" H 8575 6792 50  0000 C CNN
F 1 "TMP100" H 8575 6701 50  0000 C CNN
F 2 "Footprints:SOT-23-6" H 8575 5975 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/tmp100" H 8525 6325 50  0001 C CNN
	1    8575 6325
	1    0    0    -1  
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R49
U 1 1 6F7049E7
P 7900 5950
F 0 "R49" V 7900 5950 50  0000 C CNN
F 1 "2.2k" V 7900 5950 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 7830 5950 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 7900 5950 50  0001 C CNN
	1    7900 5950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7900 6100 7900 6125
Wire Wire Line
	8175 6125 7900 6125
Connection ~ 8050 6225
Wire Wire Line
	8050 6225 8175 6225
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C61
U 1 1 6F821404
P 9100 6250
F 0 "C61" H 8875 6250 50  0000 L CNN
F 1 "100nF 25v" H 9110 6170 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 9100 6250 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 9100 6250 50  0001 C CNN
	1    9100 6250
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0125
U 1 1 6F823561
P 9200 6400
F 0 "#PWR0125" H 9200 6150 50  0001 C CNN
F 1 "GND" V 9200 6250 50  0000 R CNN
F 2 "" H 9200 6400 50  0001 C CNN
F 3 "" H 9200 6400 50  0001 C CNN
	1    9200 6400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9200 6400 9100 6400
Wire Wire Line
	8975 6200 9000 6200
Wire Wire Line
	9000 6200 9000 6300
Wire Wire Line
	8975 6300 9000 6300
Connection ~ 9000 6300
Wire Wire Line
	9000 6300 9000 6400
Wire Wire Line
	8975 6100 9100 6100
Wire Wire Line
	9100 6100 9100 6150
Wire Wire Line
	9100 6350 9100 6400
Connection ~ 9100 6400
Text GLabel 9175 6100 2    50   Input ~ 0
3.3V
Wire Wire Line
	9175 6100 9100 6100
Connection ~ 9100 6100
Wire Wire Line
	8050 5775 9100 5775
Wire Wire Line
	9100 5775 9100 6100
Wire Wire Line
	7900 5800 7900 5775
Wire Wire Line
	7900 5775 8050 5775
Connection ~ 8050 5775
Wire Wire Line
	8875 8775 8575 8775
Wire Wire Line
	7900 8750 7900 8775
Connection ~ 7900 8775
Wire Wire Line
	7900 8775 7775 8775
Connection ~ 7900 8175
Wire Wire Line
	7900 8175 8125 8175
Wire Wire Line
	8075 7950 8075 8075
Connection ~ 8075 7950
Connection ~ 8075 8075
Wire Wire Line
	7900 7925 7900 8075
Connection ~ 7900 8075
Connection ~ 8075 7700
Wire Wire Line
	7900 7725 7900 7550
Connection ~ 7900 7550
Wire Wire Line
	7900 7550 8825 7550
Wire Wire Line
	7925 7400 8050 7400
Wire Wire Line
	7925 7075 7925 7025
Connection ~ 7925 7025
Wire Wire Line
	7925 7025 8175 7025
Wire Wire Line
	8050 6100 8050 6225
Wire Wire Line
	8050 5800 8050 5775
$Comp
L 3Phase-rescue:R-3Phase-rescue R55
U 1 1 6F704456
P 8050 5950
F 0 "R55" V 8050 5950 50  0000 C CNN
F 1 "2.2k" V 8050 5950 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 7980 5950 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 8050 5950 50  0001 C CNN
	1    8050 5950
	-1   0    0    -1  
$EndComp
Connection ~ 7900 6125
Wire Wire Line
	8975 6400 9000 6400
Connection ~ 9000 6400
Wire Wire Line
	9000 6400 9100 6400
$Comp
L 3Phase-rescue:R-3Phase-rescue R54
U 1 1 7478F6C0
P 8025 5650
F 0 "R54" V 8025 5650 50  0000 C CNN
F 1 "2.2k" V 8025 5650 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 7955 5650 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 8025 5650 50  0001 C CNN
	1    8025 5650
	0    1    -1   0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R53
U 1 1 7478FFFE
P 8025 5550
F 0 "R53" V 8025 5550 50  0000 C CNN
F 1 "2.2k" V 8025 5550 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 7955 5550 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 8025 5550 50  0001 C CNN
	1    8025 5550
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR0126
U 1 1 7482DD23
P 8225 5650
F 0 "#PWR0126" H 8225 5400 50  0001 C CNN
F 1 "GND" V 8225 5500 50  0000 R CNN
F 2 "" H 8225 5650 50  0001 C CNN
F 3 "" H 8225 5650 50  0001 C CNN
	1    8225 5650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8175 5650 8225 5650
Wire Wire Line
	7875 5650 7825 5650
Wire Wire Line
	7875 5550 7825 5550
Wire Wire Line
	7825 5550 7825 5650
Connection ~ 7825 5650
Text GLabel 8300 5550 2    50   Input ~ 0
Vin
Wire Wire Line
	8300 5550 8175 5550
Wire Wire Line
	7775 8675 7775 8775
Connection ~ 7775 8775
Wire Wire Line
	7775 8575 7775 8675
Connection ~ 7775 8675
Wire Wire Line
	7775 8475 7775 8375
Connection ~ 7775 8175
Wire Wire Line
	7775 8175 7900 8175
Connection ~ 7775 8275
Wire Wire Line
	7775 8275 7775 8175
Connection ~ 7775 8375
Wire Wire Line
	7775 8375 7775 8275
Wire Wire Line
	7900 8175 7900 8550
Wire Wire Line
	8125 8550 8125 8175
Connection ~ 8125 8175
Wire Wire Line
	8125 8175 8350 8175
Wire Wire Line
	8350 8550 8350 8175
Connection ~ 8350 8175
Wire Wire Line
	8350 8175 8575 8175
Wire Wire Line
	8575 8550 8575 8175
Connection ~ 8575 8175
Wire Wire Line
	8575 8175 8950 8175
Wire Wire Line
	8575 8750 8575 8775
Connection ~ 8575 8775
Wire Wire Line
	8575 8775 8350 8775
Wire Wire Line
	8350 8750 8350 8775
Connection ~ 8350 8775
Wire Wire Line
	8350 8775 8125 8775
Wire Wire Line
	8125 8750 8125 8775
Connection ~ 8125 8775
Wire Wire Line
	8125 8775 7900 8775
Wire Wire Line
	7900 8075 8075 8075
Text GLabel 9175 7550 2    50   Input ~ 0
3.3V
$Comp
L 3Phase-rescue:R-3Phase-rescue R51
U 1 1 764B597C
P 9000 7550
F 0 "R51" V 9000 7550 50  0000 C CNN
F 1 "2.2k" V 9000 7550 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 8930 7550 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 9000 7550 50  0001 C CNN
	1    9000 7550
	0    1    -1   0   
$EndComp
Wire Wire Line
	9150 7550 9175 7550
Wire Wire Line
	8850 7550 8825 7550
Connection ~ 8825 7550
Text GLabel 9175 7400 2    50   Input ~ 0
3.3V
Wire Wire Line
	8825 7825 8725 7825
Wire Wire Line
	7775 8775 7750 8775
Wire Wire Line
	5800 6225 6200 6225
Wire Wire Line
	6200 6525 5075 6525
Wire Wire Line
	6200 6925 5075 6925
Wire Wire Line
	5975 6725 6200 6725
Wire Wire Line
	7900 6125 7750 6125
Wire Wire Line
	7750 6225 8050 6225
Wire Wire Line
	6200 7025 5075 7025
Wire Wire Line
	6200 7225 5075 7225
Wire Wire Line
	6200 4175 6025 4175
Wire Wire Line
	5075 7125 6200 7125
Wire Wire Line
	5075 7825 6200 7825
Wire Wire Line
	5075 7725 6200 7725
Wire Wire Line
	5075 7625 6200 7625
Wire Wire Line
	5075 7525 6200 7525
Wire Wire Line
	6200 7425 5075 7425
Wire Wire Line
	5075 7325 6200 7325
Wire Wire Line
	5075 8525 6200 8525
Wire Wire Line
	5075 8625 6200 8625
Wire Wire Line
	6200 8725 5075 8725
Wire Wire Line
	6200 3825 6025 3825
Wire Wire Line
	5075 8825 6200 8825
Wire Wire Line
	6200 8925 5075 8925
Wire Wire Line
	5075 9025 6200 9025
Wire Wire Line
	6200 5550 5050 5550
Wire Wire Line
	7750 8375 7775 8375
Wire Wire Line
	7750 8675 7775 8675
Wire Wire Line
	5075 9125 6200 9125
Wire Wire Line
	5050 5450 6200 5450
Wire Wire Line
	6200 8425 5075 8425
Wire Wire Line
	5075 9225 6200 9225
Wire Wire Line
	5075 9325 6200 9325
Wire Wire Line
	6200 9425 5075 9425
Wire Wire Line
	5075 9525 6200 9525
Wire Wire Line
	5075 9625 6200 9625
Wire Wire Line
	6175 9775 6200 9775
Wire Wire Line
	7750 10175 8950 10175
Wire Wire Line
	7750 10075 8950 10075
Wire Wire Line
	6025 4575 6200 4575
Wire Wire Line
	7750 9975 8950 9975
Wire Wire Line
	8950 9875 7750 9875
Wire Wire Line
	7750 8275 7775 8275
Wire Wire Line
	5075 8325 6200 8325
Wire Wire Line
	7750 8575 7775 8575
Wire Wire Line
	7750 8875 8950 8875
Wire Wire Line
	7750 8975 8950 8975
Wire Wire Line
	7750 9075 8950 9075
Wire Wire Line
	8950 9175 7750 9175
Wire Wire Line
	6200 8225 5075 8225
Wire Wire Line
	8950 9275 7750 9275
Wire Wire Line
	7750 9375 8950 9375
Wire Wire Line
	5075 8125 6200 8125
Wire Wire Line
	7750 8175 7775 8175
Wire Wire Line
	6200 5150 5850 5150
Wire Wire Line
	6175 5250 6200 5250
Wire Wire Line
	5200 5350 6200 5350
Wire Wire Line
	4950 4375 6200 4375
Wire Wire Line
	7750 7550 7900 7550
Wire Wire Line
	8075 7700 7750 7700
Wire Wire Line
	7750 8075 7900 8075
Wire Wire Line
	6200 8025 5075 8025
Wire Wire Line
	7825 5650 7750 5650
Wire Wire Line
	7750 9475 8950 9475
Wire Wire Line
	8950 9575 7750 9575
Wire Wire Line
	7750 9675 8950 9675
Wire Wire Line
	7750 9775 8950 9775
Wire Wire Line
	6125 3675 6200 3675
Wire Wire Line
	8050 6925 7750 6925
Wire Wire Line
	7750 7025 7925 7025
Wire Wire Line
	5075 6625 6200 6625
Wire Wire Line
	7750 8475 7775 8475
Wire Wire Line
	5075 6825 6200 6825
Wire Wire Line
	5075 7925 6200 7925
$Comp
L 3Phase-rescue:STM32L412R8T6-Components U7
U 1 1 6148C9C3
P 6900 5175
F 0 "U7" H 7600 7350 50  0000 C CNN
F 1 "STM32L412R8T6" H 6600 7350 50  0000 C CNN
F 2 "Package_QFP:LQFP-100_14x14mm_P0.5mm" H 6300 3475 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00213872.pdf" H 6900 5175 50  0001 C CNN
	1    6900 5175
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 5450 7750 5450
Wire Wire Line
	7750 5350 8600 5350
Wire Wire Line
	8600 5250 7750 5250
Wire Wire Line
	7750 5150 8600 5150
Wire Wire Line
	8600 5050 7750 5050
Wire Wire Line
	7750 4950 8600 4950
Wire Wire Line
	8600 4850 7750 4850
Wire Wire Line
	7750 4750 8600 4750
Wire Wire Line
	8600 4650 7750 4650
Wire Wire Line
	7750 4550 8600 4550
Wire Wire Line
	7750 4450 8600 4450
Wire Wire Line
	8600 4350 7750 4350
Wire Wire Line
	5050 6225 5100 6225
Wire Wire Line
	5800 6125 6125 6125
Wire Wire Line
	6125 6100 6125 6125
Connection ~ 6125 6125
Wire Wire Line
	6125 6125 6200 6125
$Comp
L Device:LED_Small_ALT D9
U 1 1 6890644B
P 6000 5650
F 0 "D9" H 5900 5700 50  0000 C CNN
F 1 "LED_Small_ALT" H 6000 5534 50  0001 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" V 6000 5650 50  0001 C CNN
F 3 "~" V 6000 5650 50  0001 C CNN
	1    6000 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 5650 6125 5650
Wire Wire Line
	6125 5650 6125 5800
$Comp
L 3Phase-rescue:R-3Phase-rescue R62
U 1 1 795BCADF
P 7975 6725
F 0 "R62" V 7975 6725 50  0000 C CNN
F 1 "2.2k" V 7975 6725 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 7905 6725 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 7975 6725 50  0001 C CNN
	1    7975 6725
	0    1    -1   0   
$EndComp
$Comp
L 3Phase-rescue:R-3Phase-rescue R63
U 1 1 795C46F8
P 7975 6825
F 0 "R63" V 7975 6825 50  0000 C CNN
F 1 "2.2k" V 7975 6825 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 7905 6825 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 7975 6825 50  0001 C CNN
	1    7975 6825
	0    1    -1   0   
$EndComp
Wire Wire Line
	7750 6725 7825 6725
Wire Wire Line
	7750 6825 7825 6825
Wire Wire Line
	8125 6725 8175 6725
Wire Wire Line
	8125 6825 8150 6825
Text GLabel 8400 7475 2    50   Input ~ 0
2.5V
Wire Wire Line
	8400 7475 8075 7475
Wire Wire Line
	8075 7475 8075 7700
Wire Wire Line
	4925 5775 5075 5775
Wire Wire Line
	5075 5775 5075 6025
Wire Wire Line
	5075 6025 5050 6025
Text GLabel 5850 5775 2    50   Input ~ 0
3.3V
Text GLabel 5075 8825 0    50   Input ~ 0
IN5_G3
Text GLabel 5075 8725 0    50   Input ~ 0
IN5_G2
Text GLabel 5075 8925 0    50   Input ~ 0
IN5_G4
Text GLabel 5075 8625 0    50   Input ~ 0
IN5_G1
Text GLabel 5075 8025 0    50   Input ~ 0
IN7_G4
Text GLabel 5075 8525 0    50   Input ~ 0
IN5_G0
Text GLabel 5075 9125 0    50   Input ~ 0
IN4_ERR
$Comp
L Transistor_FET:Si2319CDS Q1
U 1 1 60FDA6DE
P 5425 1525
F 0 "Q1" V 5675 1525 50  0000 C CNN
F 1 "Si2319CDS" V 5765 1525 50  0001 C CNN
F 2 "Footprints:SOT-23" H 5625 1450 50  0001 L CIN
F 3 "http://www.vishay.com/docs/66709/si2319cd.pdf" H 5425 1525 50  0001 L CNN
	1    5425 1525
	0    1    1    0   
$EndComp
$Comp
L Components:USB_A J8
U 1 1 6D665A3F
P 8100 2375
F 0 "J8" H 7870 2446 50  0000 R CNN
F 1 "USB_A" H 7870 2355 50  0000 R CNN
F 2 "Footprints:USB_A_Molex_67643_Horizontal" H 8250 2325 50  0001 C CNN
F 3 " ~" H 8250 2325 50  0001 C CNN
	1    8100 2375
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7475 2350 7675 2350
$Comp
L Device:Ferrite_Bead_Small FB1
U 1 1 6FD3910C
P 7550 2150
F 0 "FB1" V 7425 2150 50  0000 C CNN
F 1 "BLM18KG601SZ1D" V 7404 2150 50  0001 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" V 7480 2150 50  0001 C CNN
F 3 "~" H 7550 2150 50  0001 C CNN
	1    7550 2150
	0    1    1    0   
$EndComp
Wire Wire Line
	7675 2250 7475 2250
Wire Wire Line
	7675 2450 7650 2450
Wire Wire Line
	6875 2150 7000 2150
Wire Wire Line
	7650 2150 7675 2150
Wire Wire Line
	8975 7125 8975 7250
Text GLabel 9075 6925 2    50   Input ~ 0
USB_DM
Text GLabel 9075 7025 2    50   Input ~ 0
USB_DP
Wire Wire Line
	9075 7025 8950 7025
Wire Wire Line
	9075 6925 8950 6925
Text GLabel 7475 2250 0    50   Input ~ 0
USB_DM
Text GLabel 7475 2350 0    50   Input ~ 0
USB_DP
$Comp
L power:GND #PWR0108
U 1 1 616CB91C
P 6900 2575
F 0 "#PWR0108" H 6900 2325 50  0001 C CNN
F 1 "GND" H 6850 2550 50  0000 R CNN
F 2 "" H 6900 2575 50  0001 C CNN
F 3 "" H 6900 2575 50  0001 C CNN
	1    6900 2575
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 2450 7650 2550
Connection ~ 7650 2550
Wire Wire Line
	7650 2550 7675 2550
$Comp
L Components:MIC2005A-2YM6 U19
U 1 1 619FF1B5
P 6475 2250
F 0 "U19" H 6475 2617 50  0000 C CNN
F 1 "MIC2005A-2YM6" H 6475 2526 50  0000 C CNN
F 2 "Footprints:SOT-23-5" H 7275 2000 50  0001 C CNN
F 3 "https://ww1.microchip.com/downloads/en/DeviceDoc/MIC2025-2075-Single-Channel-Power-Distribution-Switch-DS20006030A.pdf" H 6475 2250 50  0001 C CNN
	1    6475 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6875 2350 6900 2350
Wire Wire Line
	6900 2350 6900 2550
Connection ~ 6900 2550
Wire Wire Line
	6900 2550 6900 2575
Wire Wire Line
	6900 2550 7000 2550
Text GLabel 5550 2150 0    50   Input ~ 0
5V
Wire Wire Line
	5550 2150 5600 2150
$Comp
L Device:CP_Small C63
U 1 1 61CA9BD1
P 7000 2375
F 0 "C63" H 7088 2375 50  0000 L CNN
F 1 "CP_Small" H 7088 2330 50  0001 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3528-21_Kemet-B" H 7000 2375 50  0001 C CNN
F 3 "~" H 7000 2375 50  0001 C CNN
	1    7000 2375
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 2475 7000 2550
Connection ~ 7000 2550
Wire Wire Line
	7000 2550 7650 2550
Wire Wire Line
	7000 2275 7000 2150
Connection ~ 7000 2150
Wire Wire Line
	7000 2150 7450 2150
Wire Wire Line
	6075 2250 5950 2250
$Comp
L 3Phase-rescue:R-3Phase-rescue R65
U 1 1 62271818
P 5775 2250
F 0 "R65" V 5775 2250 50  0000 C CNN
F 1 "2.2k" V 5775 2250 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5705 2250 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 5775 2250 50  0001 C CNN
	1    5775 2250
	0    -1   1    0   
$EndComp
Wire Wire Line
	5625 2250 5600 2250
Wire Wire Line
	5600 2250 5600 2150
Connection ~ 5600 2150
Wire Wire Line
	5600 2150 6075 2150
Text GLabel 6050 2500 3    50   Input ~ 0
USB_FLG
Text GLabel 5950 2500 3    50   Input ~ 0
USB_EN
Wire Wire Line
	6075 2350 6050 2350
Wire Wire Line
	6050 2350 6050 2500
$Comp
L 3Phase-rescue:R-3Phase-rescue R66
U 1 1 62A5305E
P 5775 2350
F 0 "R66" V 5775 2350 50  0000 C CNN
F 1 "2.2k" V 5775 2350 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5705 2350 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 5775 2350 50  0001 C CNN
	1    5775 2350
	0    -1   1    0   
$EndComp
Wire Wire Line
	5950 2500 5950 2250
Connection ~ 5950 2250
Wire Wire Line
	5950 2250 5925 2250
Wire Wire Line
	5925 2350 6050 2350
Connection ~ 6050 2350
Wire Wire Line
	5625 2350 5600 2350
Wire Wire Line
	5600 2350 5600 2250
Connection ~ 5600 2250
Text GLabel 5775 1625 2    50   Input ~ 0
5V
Wire Wire Line
	5625 1625 5775 1625
$Comp
L 3Phase-rescue:R-3Phase-rescue R64
U 1 1 63452116
P 5175 1450
F 0 "R64" V 5175 1450 50  0000 C CNN
F 1 "2.2k" V 5175 1450 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5105 1450 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 5175 1450 50  0001 C CNN
	1    5175 1450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5175 1600 5175 1625
Connection ~ 5175 1625
Wire Wire Line
	5175 1625 4650 1625
Wire Wire Line
	5175 1300 5175 1275
Wire Wire Line
	5175 1275 5425 1275
Wire Wire Line
	5425 1275 5425 1325
Text GLabel 6075 1050 2    50   Input ~ 0
EN
$Comp
L Transistor_BJT:MMBT3904 Q2
U 1 1 63715AE7
P 5525 1050
F 0 "Q2" H 5716 1050 50  0000 L CNN
F 1 "MMBT3904" H 5716 1095 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5725 975 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/2N3903-D.PDF" H 5525 1050 50  0001 L CNN
	1    5525 1050
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 637D8E92
P 5425 825
F 0 "#PWR0110" H 5425 575 50  0001 C CNN
F 1 "GND" H 5300 800 50  0000 R CNN
F 2 "" H 5425 825 50  0001 C CNN
F 3 "" H 5425 825 50  0001 C CNN
	1    5425 825 
	-1   0    0    1   
$EndComp
Wire Wire Line
	5425 825  5425 850 
Wire Wire Line
	5425 1250 5425 1275
Connection ~ 5425 1275
$Comp
L 3Phase-rescue:R-3Phase-rescue R73
U 1 1 639317A4
P 5900 1050
F 0 "R73" V 5900 1050 50  0000 C CNN
F 1 "2.2k" V 5900 1050 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 5830 1050 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 5900 1050 50  0001 C CNN
	1    5900 1050
	0    1    -1   0   
$EndComp
Wire Wire Line
	5725 1050 5750 1050
Wire Wire Line
	6050 1050 6075 1050
Wire Wire Line
	3075 625  3475 625 
Text GLabel 5750 3550 0    50   Input ~ 0
USB_EN
Text GLabel 5750 3450 0    50   Input ~ 0
USB_FLG
Wire Wire Line
	5750 3450 6200 3450
Wire Wire Line
	6200 3550 5750 3550
$Comp
L Connector:Conn_01x01_Female J16
U 1 1 648A9C4E
P 8800 5450
F 0 "J16" H 8828 5476 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 5385 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 5450 50  0001 C CNN
F 3 "~" H 8800 5450 50  0001 C CNN
	1    8800 5450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J15
U 1 1 648B0629
P 8800 5350
F 0 "J15" H 8828 5376 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 5285 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 5350 50  0001 C CNN
F 3 "~" H 8800 5350 50  0001 C CNN
	1    8800 5350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J14
U 1 1 648B147B
P 8800 5250
F 0 "J14" H 8828 5276 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 5185 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 5250 50  0001 C CNN
F 3 "~" H 8800 5250 50  0001 C CNN
	1    8800 5250
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J13
U 1 1 648B1C59
P 8800 5150
F 0 "J13" H 8828 5176 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 5085 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 5150 50  0001 C CNN
F 3 "~" H 8800 5150 50  0001 C CNN
	1    8800 5150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J12
U 1 1 648B24C2
P 8800 5050
F 0 "J12" H 8828 5076 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 4985 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 5050 50  0001 C CNN
F 3 "~" H 8800 5050 50  0001 C CNN
	1    8800 5050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J11
U 1 1 648B2D49
P 8800 4950
F 0 "J11" H 8828 4976 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 4885 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 4950 50  0001 C CNN
F 3 "~" H 8800 4950 50  0001 C CNN
	1    8800 4950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J10
U 1 1 648B35AB
P 8800 4850
F 0 "J10" H 8828 4876 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 4785 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 4850 50  0001 C CNN
F 3 "~" H 8800 4850 50  0001 C CNN
	1    8800 4850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J9
U 1 1 648B3DC1
P 8800 4750
F 0 "J9" H 8828 4776 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 4685 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 4750 50  0001 C CNN
F 3 "~" H 8800 4750 50  0001 C CNN
	1    8800 4750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J31
U 1 1 648B83B1
P 8800 4650
F 0 "J31" H 8828 4676 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 4585 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 4650 50  0001 C CNN
F 3 "~" H 8800 4650 50  0001 C CNN
	1    8800 4650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J30
U 1 1 648B8C41
P 8800 4550
F 0 "J30" H 8828 4576 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 4485 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 4550 50  0001 C CNN
F 3 "~" H 8800 4550 50  0001 C CNN
	1    8800 4550
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J29
U 1 1 648B9481
P 8800 4450
F 0 "J29" H 8828 4476 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 4385 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 4450 50  0001 C CNN
F 3 "~" H 8800 4450 50  0001 C CNN
	1    8800 4450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J28
U 1 1 648B9D98
P 8800 4350
F 0 "J28" H 8828 4376 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 4285 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 4350 50  0001 C CNN
F 3 "~" H 8800 4350 50  0001 C CNN
	1    8800 4350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J27
U 1 1 648BC46F
P 8800 4250
F 0 "J27" H 8828 4276 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 4185 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 4250 50  0001 C CNN
F 3 "~" H 8800 4250 50  0001 C CNN
	1    8800 4250
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J26
U 1 1 648BCD54
P 8800 4150
F 0 "J26" H 8828 4176 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 4085 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 4150 50  0001 C CNN
F 3 "~" H 8800 4150 50  0001 C CNN
	1    8800 4150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J25
U 1 1 648BD694
P 8800 4050
F 0 "J25" H 8828 4076 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 3985 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 4050 50  0001 C CNN
F 3 "~" H 8800 4050 50  0001 C CNN
	1    8800 4050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J24
U 1 1 648BE050
P 8800 3950
F 0 "J24" H 8828 3976 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 3885 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 3950 50  0001 C CNN
F 3 "~" H 8800 3950 50  0001 C CNN
	1    8800 3950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J23
U 1 1 648BEA17
P 8800 3850
F 0 "J23" H 8828 3876 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 3785 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 3850 50  0001 C CNN
F 3 "~" H 8800 3850 50  0001 C CNN
	1    8800 3850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J22
U 1 1 648BF3BE
P 8800 3750
F 0 "J22" H 8828 3776 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 3685 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 3750 50  0001 C CNN
F 3 "~" H 8800 3750 50  0001 C CNN
	1    8800 3750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J21
U 1 1 648BFCE1
P 8800 3650
F 0 "J21" H 8828 3676 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 3585 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 3650 50  0001 C CNN
F 3 "~" H 8800 3650 50  0001 C CNN
	1    8800 3650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J20
U 1 1 648C0627
P 8800 3550
F 0 "J20" H 8828 3576 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 3485 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 3550 50  0001 C CNN
F 3 "~" H 8800 3550 50  0001 C CNN
	1    8800 3550
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J17
U 1 1 648C2387
P 8800 3450
F 0 "J17" H 8828 3476 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 3385 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 3450 50  0001 C CNN
F 3 "~" H 8800 3450 50  0001 C CNN
	1    8800 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 4250 8600 4250
Wire Wire Line
	8600 4150 7750 4150
Wire Wire Line
	7750 4050 8600 4050
Wire Wire Line
	8600 3950 7750 3950
Wire Wire Line
	7750 3850 8600 3850
Wire Wire Line
	8600 3750 7750 3750
Wire Wire Line
	7750 3650 8600 3650
Wire Wire Line
	8600 3550 7750 3550
$Comp
L power:GND #PWR0122
U 1 1 64F7F627
P 8550 3450
F 0 "#PWR0122" H 8550 3200 50  0001 C CNN
F 1 "GND" H 8500 3425 50  0000 R CNN
F 2 "" H 8550 3450 50  0001 C CNN
F 3 "" H 8550 3450 50  0001 C CNN
	1    8550 3450
	0    1    1    0   
$EndComp
Wire Wire Line
	8550 3450 8575 3450
$Comp
L Connector:Conn_01x01_Female J32
U 1 1 6514E428
P 8800 3350
F 0 "J32" H 8828 3376 50  0001 L CNN
F 1 "Conn_01x01_Female" H 8828 3285 50  0001 L CNN
F 2 "Footprints:TestPoint_1.8mm" H 8800 3350 50  0001 C CNN
F 3 "~" H 8800 3350 50  0001 C CNN
	1    8800 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 3350 8575 3350
Wire Wire Line
	8575 3350 8575 3450
Connection ~ 8575 3450
Wire Wire Line
	8575 3450 8600 3450
$Comp
L 3Phase-rescue:R-3Phase-rescue R59
U 1 1 610C5707
P 7975 6625
F 0 "R59" V 7975 6625 50  0000 C CNN
F 1 "2.2k" V 7975 6625 50  0001 C CNN
F 2 "Footprints:R_0603_1608Metric" V 7905 6625 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_2-2KR-2201-1_C114662.html" H 7975 6625 50  0001 C CNN
	1    7975 6625
	0    1    -1   0   
$EndComp
Wire Wire Line
	8150 6825 8150 6625
Wire Wire Line
	8150 6625 8125 6625
Connection ~ 8150 6825
Wire Wire Line
	8150 6825 8175 6825
Wire Wire Line
	7825 6625 7750 6625
$Comp
L Components:74LVC2T45DC U13
U 1 1 610C162C
P 5450 6325
F 0 "U13" H 5350 6200 50  0000 C CNN
F 1 "74LVC2T45DC" H 5450 6776 50  0001 C CNN
F 2 "Footprints:VSSOP-8_2.3x2mm_P0.5mm" H 5450 5475 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74LVC_LVCH2T45.pdf" H 5700 6075 50  0001 C CNN
	1    5450 6325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5100 6325 5075 6325
Wire Wire Line
	5100 6025 5075 6025
Connection ~ 5075 6025
Wire Wire Line
	5075 6350 5075 6325
Connection ~ 5075 6325
Wire Wire Line
	5075 6325 5050 6325
Wire Wire Line
	5800 6325 6200 6325
Wire Wire Line
	5050 5650 5450 5650
$Comp
L 3Phase-rescue:C_Small-3Phase-rescue C62
U 1 1 617F2593
P 5200 5775
F 0 "C62" V 5150 5825 50  0000 L CNN
F 1 "100nF 25v" H 5210 5695 50  0001 L CNN
F 2 "Footprints:C_0603_1608Metric" H 5200 5775 50  0001 C CNN
F 3 "https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_SAMSUNG_CL10B104KA8NNNC_100nF-104-10-25V_C1590.html" H 5200 5775 50  0001 C CNN
	1    5200 5775
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR0124
U 1 1 617F5C6C
P 5450 5825
F 0 "#PWR0124" H 5450 5575 50  0001 C CNN
F 1 "GND" H 5325 5750 50  0000 C CNN
F 2 "" H 5450 5825 50  0000 C CNN
F 3 "" H 5450 5825 50  0000 C CNN
	1    5450 5825
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 5825 5450 5775
Connection ~ 5450 5650
Wire Wire Line
	5450 5650 5900 5650
Wire Wire Line
	5300 5775 5450 5775
Connection ~ 5450 5775
Wire Wire Line
	5450 5775 5450 5650
Wire Wire Line
	5100 5775 5075 5775
Connection ~ 5075 5775
Wire Wire Line
	5450 5775 5600 5775
Wire Wire Line
	5800 5775 5825 5775
Wire Wire Line
	5800 6025 5825 6025
Wire Wire Line
	5825 6025 5825 5775
Connection ~ 5825 5775
Wire Wire Line
	5825 5775 5850 5775
$EndSCHEMATC
